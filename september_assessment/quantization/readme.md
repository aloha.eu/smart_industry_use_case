# Quantization

For this part you are going to need:
* an onnx file for a trained model produced by ALOHA using the `reply`, because of the use of predefined split and preprocessing pipelines that are not present on other branches as of now
* a json file describing the preprocessing pipeline that has been used during the training of the model, you can find the corresponding document in the pipeline collection in mongodb and save it in a json. **If you are using plugins that reference a folder/path, adjust the path in the json to be coherent with your local environment!**
* a json file describing the dataset that has been used to train the model, you can find also this one in mongo and download the corresponding document
* the hdf5 dataset file on which the model has been trained, notice that it is only one file containing both training and validation set, the dataset json file is used to store how it is splitted
* remove from the 

As `regime.json` use the following

```json
{
"lr": 0.00001,
"epochs": 3,
"batch_size": 100,
"weight_decay": 0.0005,
"optimizer": "adam",
"lr_decay": 0.98,
"relaxation": {
"0": {
"W_bits": 7.0,
"x_bits": 8.0
},
"for_epochs": 5,
"for_epochs_no_abs_bound": 5,
"delta_loss_less_than": 5,
"bit_scaler": 3.0,
"bit_stop_condition": 9.0,
"running_avg_memory": 5,
"delta_loss_running_std_stale": 1.5e4,
"abs_loss_stale": 1.5e4,
"scale_lr": true,
"lr_scaler": 1.4142135624
}
}
```

Once you have collected all these files you can procede with the instructions below.

## Setup

Move to `refinement_for_parsimonius_inference` in your ALOHA installation

Checkout branch `reply`

```
git pull
git checkout reply
```

Now, inside refinement_for_parsimonius_inference move to `./app/rpi_engine`

Checkout branch `reply`
```
git pull
git checkout reply
```

Clone `onnxparser` in the `rpi_engine` directory and then checkout branch `master`
```
git clone git@gitlab.com:aloha.eu/onnxparser.git
cd onnxparser
git checkout master
cd ..
```

Create a python virtual environment in `rpi_engine`
```
sudo apt install python3-venv
python3 -m venv reply-rpi-env
```

Now enter in the newly created virtual environment
```
source reply-rpi-env/bin/activate
```
If you need to exit the virtual environment you can use
```
deactivate
```
Don't do it for now!

Install `torch==1.4.0` and `torchvision==0.5.0` in the virtual environment, build of nemo is failing for pytorch 1.5.0!

```
pip3 install torch==1.4.0 torchvision==0.5.0 tqdm h5py onnx tensorboardX numba==0.48 librosa
```

## How to use

Move to `refinement_for_parsimonius_inference/app/rpi_engine`, then activate the virtual environment previously created

```
source reply-rpi-env/bin/activate
```

Add plugins and onnparser to python path, your path will slightly differ! Pay attention to use the right one.

```
export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/shared_data"

export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/refinement_for_parsimonious_inference/app/rpi_engine/onnxparser"
```

In the following command replace paths and file names with your owns.n In this case dataset.json is the json from the entry in the dataset collection in the mongo db from which all the fields except slice_split have been removed, ./audio_to_mel_3216.json contains the document from the db for the pipeline.
If you don't know which is the `input_range` you can use the suggestions in the next section to find it.

```
python3 rpi_engine.py --onnx_path 7b.onnx --regime regime.json --log_dir . --export --dataset test_reply_projectID-5f2c1397394894ac14b96a94.hdf5 --split_json dataset.json --path_preprocessing_pipeline_instance ./audio_to_mel_3216.json --input_shape "[1,32,16]" --input_range "[-32, 32]"
```

## Find input range

You can modify the script `plugins/tests/test_multiple_load_and_apply_transforms.py` to compute mean and standard deviation based on the entire dataset or a subset of it.

## Comparison regular vs quantized

Clone the `smart_industry_use_case` repository if you haven't done that already, `cd` into `september_assessment/scripts/`.

Add `plugins` and `onnparser` to python path
```
export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/shared_data"

export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/refinement_for_parsimonious_inference/app/rpi_engine/onnxparser"
```

Then you can use 

```
python3 comparison_quantized.py R3bmaxnobntrained.onnx quantized_model_8_7_melstnobn.onnx melst3216aug.json --sample_file /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/0ac15fe9_nohash_0.wav -d 3
```