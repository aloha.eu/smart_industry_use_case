# September assessment: REPLY KWS with TE v2 and plugins

The goal of this assessment is to evaluate the ALOHA tool flow on 2 possible implementations of the KWS use case: one with 6 classes and one with 13 classes.

## 6 classes: 4 keywords + noise and unknown

```
["yes", "no", "go", "stop", "noise", "unknown"]
```

## 13 classes: 11 keywords + noise and unknown

```
["one", "two", "three", "four", "five", "six", "seven", "forward", "backward", "go", "stop", "unknown", "noise"]
```