# How we used these scripts

Use these commands from the `smart_industry_usecase/august_assessment/scripts` directory.

An help menu is available for all the scripts using `-h`.

Change paths with your owns

## Prepare silence/noise

Prepare the noise class samples: use seed 30 and add the 4 more noises we added to the `_background_noise_` folder (they are in `smart_industry_usecase/august_assessment/dataset_files/add to _background_noise_/`).

`python3 prepare_silence.py /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/_background_noise_/ /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/noise/ 5000 -l 16000 -sr 16000 -s 30`

## Commands for 6 classes experiments

Prepare the dataset file, configure the keywords6.json first if you don't have it, in this case:

```json
{
    "keywords" : ["yes", "no", "go", "stop", "noise", "unknown"],
    "ignore_for_unknown" : ["noise", "silence"]
}
```

`python3 dataset_file.py -s 30 /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/ /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply6.txt  /opt/www/shared_data/usecases/REPLY_KWS/ 7000 keywords6.json`

Split the dataset file, note that the validation set file name should be the same as training plus `_validation` to work with our TEv2 hack (as in the example below).

The dataset is splitted looking at the speaker hash in the file names, classes "noise" and "unknown" do not apply this criterion (`-i 4 5`)

`python3 split_kws.py -i 4 5 -s 30 /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply6.txt /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply6_splitted.txt /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply6_splitted_validation.txt 0.7`

## Commands for 13 classes experiments

Prepare the dataset file, configure the keywords6.json first if you don't have it, in this case:

```json
{
    "keywords" : ["one", "two", "three", "four", "five", "six", "seven", "forward", "backward", "go", "stop", "unknown", "noise"],
    "ignore_for_unknown" : ["noise", "silence"]
}
```

`python3 dataset_file.py -s 30 /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/ /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply13.txt  /opt/www/shared_data/usecases/REPLY_KWS/ 7000 keywords13.json`

Split the dataset file, note that the validation set file name should be the same as training plus `_validation` to work with our TEv2 hack (as in the example below).

The dataset is splitted looking at the speaker hash in the file names, classes "noise" and "unknown" do not apply this criterion (`-i 11 12`)

`python3 split_kws.py -i 11 12 -s 30 /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply13.txt /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply13_splitted.txt /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/new_reply13_splitted_validation.txt 0.7`