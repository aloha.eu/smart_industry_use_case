import os
import random
import argparse
import json


# Walk into directories in filesystem
# Ripped from os module and slightly modified
# for alphabetical sorting
#
def sortedWalk(top, topdown=True, onerror=None):
    from os.path import join, isdir, islink
 
    names = os.listdir(top)
    names.sort()
    dirs, nondirs = [], []
 
    for name in names:
        if isdir(os.path.join(top, name)):
            dirs.append(name)
        else:
            nondirs.append(name)
 
    if topdown:
        yield top, dirs, nondirs
    for name in dirs:
        path = join(top, name)
        if not os.path.islink(path):
            for x in sortedWalk(path, topdown, onerror):
                yield x
    if not topdown:
        yield top, dirs, nondirs

def prepare_dataset_file(top, output_file, aloha_folder, max_number_of_items_per_class, keywords_json, random_seed):
    i = 0
    with open(keywords_json, 'r') as file:  
        k_json = json.load(file)
    
    keywords = k_json['keywords']
    ignore_for_unknown = k_json['ignore_for_unknown']
    dataset = [[] for x in range(len(keywords))]

    random.seed(random_seed)

    for root, dirs, files in sortedWalk(top, topdown=False):
        if root.endswith('_'):
            print('skipping', root)
        else:
            files.sort()
            for f in files:
                if f.endswith('.wav'):
                    dir_name = root.split('/')[-1]
                    if ('unknown' in keywords and dir_name not in ignore_for_unknown) or dir_name in keywords:
                        target = keywords.index(dir_name if dir_name in keywords else 'unknown')
                        path = root.replace(top, aloha_folder)+"/"+f
                        i = i+1
                        dataset[target].append(path)

    t = 0
    a = 0
    f = open(output_file, 'w')

    for d in dataset:
        if len(d) > max_number_of_items_per_class:
            random.seed(random_seed)
            d = random.sample(d, max_number_of_items_per_class)
            d.sort()
        for l in d:
            f.write(l+" "+str(t)+"\n")
            a = a + 1
        t = t + 1

    f.close()
    print(i, 'files candidate')
    print(a, 'files added to dataset')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare dataset file.')
    parser.add_argument('dataset_folder', type=str, help='Folder containing the folders containing the dataset samples.')
    parser.add_argument('output_file', type=str, help='Output dataset file.')
    parser.add_argument('aloha_folder', type=str, help='Dataset folder path inside ALOHA toolflow.')
    parser.add_argument('number_of_samples', type=int, help='Number of samples to add per class.')
    parser.add_argument('keywords_json', type=str, help='Path to json file containing selected keywords')
    parser.add_argument('-s', '--seed', dest='random_seed', type=int, help='Integer random seed.', default=None, required=False)

    args = parser.parse_args()
    prepare_dataset_file(args.dataset_folder, args.output_file, args.aloha_folder, args.number_of_samples, args.keywords_json, args.random_seed)