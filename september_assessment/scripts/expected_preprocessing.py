import argparse

import json
from torchvision.transforms import Compose
import random

import numpy as np

from plugins.utils.pipeline import Pipeline

def expected_output(audio_file, pipeline_json, decimal_bits):

    with open(pipeline_json) as f:
        pre_pipeline_instance = json.load(f)

    pipe = Pipeline(pre_pipeline_instance)

    sample_transforms = pipe.get_transforms('sample_transforms', validation=True)

    sample_transforms_composed = Compose(sample_transforms)

    sample_pipe_test_out = sample_transforms_composed(audio_file)

    batch_transforms = pipe.get_transforms('batch_transforms', validation=True)

    batch_transforms_composed = Compose(batch_transforms)

    batch_pipe_test_out = batch_transforms_composed(sample_pipe_test_out)

    expected_out = (batch_pipe_test_out.detach().numpy() * (2**decimal_bits))

    expected_out = np.rint(expected_out)

    expected_out = expected_out.astype(int)

    #TODO add clip

    return(expected_out)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute the sensortile expected preprocessing output given a preprocessing pipeline instance (json), an audio sample and a number of decimal bits.')
    parser.add_argument('audio_file', type=str, help='Path to sample')
    parser.add_argument('pipeline_json', type=str, help='Json describing pipeline')
    parser.add_argument('-d', '--decimal_bits', dest='decimal_bits', type=int, help='Number of decimal bits', default=0, required=False)
    parser.add_argument('-e', '--export_h', dest='export_h', type=str, help='Path file on which to export preprocessing result in .h format', default=None, required=False)

    args = parser.parse_args()
    result = expected_output(args.audio_file, args.pipeline_json, args.decimal_bits)

    print(result)

    if args.export_h is not None:
        f = open(args.export_h, "w")
        f.write("int8_t input["+str(len(result.flatten()))+"]={")

        for i, s in enumerate(result.flatten()):
            f.write(str(s))
            if i==len(result.flatten())-1:
                f.write('};\n')
            else:
                f.write(',\n')

        f.close()