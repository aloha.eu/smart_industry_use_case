import argparse
import json
from torchvision.transforms import Compose
import random
import numpy as np
from plugins.utils.pipeline import Pipeline
import torch
from onnxparser import onnxextract
import logging
import nemo

import matplotlib
import matplotlib.pyplot as plt

import sys

np.set_printoptions(threshold=sys.maxsize)

def preprocessing(audio_file, pipeline_json, integer=False, decimal_bits=None):

    with open(pipeline_json) as f:
        pre_pipeline_instance = json.load(f)

    pipe = Pipeline(pre_pipeline_instance)
    sample_transforms = pipe.get_transforms('sample_transforms', validation=True)
    sample_transforms_composed = Compose(sample_transforms)
    sample_pipe_test_out = sample_transforms_composed(audio_file)
    batch_transforms = pipe.get_transforms('batch_transforms', validation=True)
    batch_transforms_composed = Compose(batch_transforms)
    batch_pipe_test_out = batch_transforms_composed(sample_pipe_test_out)

    if integer:
        expected_out = batch_pipe_test_out.detach().numpy()
        if decimal_bits:
            expected_out = (expected_out * (2**decimal_bits))
        expected_out = np.rint(expected_out)
        expected_out = expected_out.astype(int)
        expected_out = np.clip(expected_out, -128, 127) #FIXME clip is hardcoded
        print(expected_out)
        return expected_out
    else:
        return batch_pipe_test_out


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compare layer by layer the outputs for the regular model and its quantized version')
    parser.add_argument('regular_model', type=str, help='Path to regular non-quantized model')
    parser.add_argument('quantized_model', type=str, help='Path to quantized model')
    parser.add_argument('pipeline_json', type=str, help='Path to json file describing pipeline')
    parser.add_argument('--sample_file', type=str, help='Path to single sample file', default=None, required=False)
    parser.add_argument('--dataset_hdf5', type=str, help='Path to hdf5 dataset', default=None, required=False)
    parser.add_argument('--dataset_json', type=str, help='Path to json file describing dataset split', default=None, required=False)
    parser.add_argument('-d', '--decimal_bits', dest='decimal_bits', type=int, help='Number of decimal bits', default=0, required=False)

    args = parser.parse_args()

    # Model construction and quantization
    logging.info('Building PyTorch model from regular ONNX')
    onnxextract(args.regular_model, path="onnxgennet", target="pytorch")
    import onnxgennet.Net as gennet
    net = gennet.Net()
    logging.info('Building PyTorch model from regular ONNX')
    #onnxextract(args.quantized_model, path="onnxgennet_quantized", target="pytorch")
    import onnxgennet_quantized.Net as gennet
    quantized_net = gennet.Net()

    def hook_fn(module, input, output):
        module.collected_output = output
        module.collected_input = input

    def hook_fn_q(module, inp, output):
        output = output.detach().numpy()
        inp = inp[0].detach().numpy()
        module.collected_output = output
        module.collected_input = inp
        print(module.__class__.__name__)
        print("out")
        print(np.rint(output))
        print("in")
        print(np.rint(inp))
        input()

    hooks = {}
    for name, module in net.named_modules():
        hooks[name] = module.register_forward_hook(hook_fn)

    hooks_quantized = {}
    for name, module in quantized_net.named_modules():
        hooks_quantized[name] = module.register_forward_hook(hook_fn_q)

    preprocessed_sample = preprocessing(args.sample_file, args.pipeline_json)
    print("Decimal bits = "+str(args.decimal_bits))
    preprocessed_sample_integer = preprocessing(args.sample_file, args.pipeline_json, True, args.decimal_bits)

    preprocessed_sample = torch.from_numpy(np.expand_dims(preprocessed_sample, axis=0)).float()
    preprocessed_sample_integer = torch.from_numpy(np.expand_dims(preprocessed_sample_integer, axis=0)).float()

    net(preprocessed_sample)

    quantized_net(preprocessed_sample_integer)

    quantized_modules = dict(quantized_net.named_modules())

    for name, module in net.named_modules():
        output_regular = torch.flatten(module.collected_output)
        output_regular = output_regular.detach().numpy()
        fig, ax = plt.subplots()
        ax.plot(range(len(output_regular)), output_regular)

        if name in list(map(lambda x: x[0], quantized_net.named_modules())):
            quantized_module = quantized_modules[name]
            output_quantized = torch.flatten(quantized_module.collected_output)
            output_quantized = output_quantized.detach().numpy()
            ax.plot(range(len(output_quantized)), output_quantized)

        ax.set(xlabel='position', ylabel='value',
            title=name)
        ax.grid()

        fig.savefig("test.png")
        plt.show()
    '''
    quantized_net = quantized_net.cuda()

    quantized_net_i = nemo.transform.quantize_pact(quantized_net, dummy_input=preprocessed_sample_integer.cuda())
    
    quantized_net_i = nemo.transform.bn_to_identity(quantized_net_i) # necessary because folding does not physically remove BN layers
    quantized_net_i.qd_stage(eps_in=1./64)
    print(quantized_net_i)
    quantized_net_i.id_stage()
    print(quantized_net_i)
    
    quantized_net_i(preprocessed_sample_integer.cuda())    
    '''