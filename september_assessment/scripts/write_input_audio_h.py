import argparse
import librosa
import numpy as np

def write_input_audio_h(filename_in, filename_out):

    signal, _ = librosa.load(filename_in, mono=False, sr=None, dtype=np.int16)

    f = open(filename_out, "a")
    f.write("int16_t input_sample[16000]={")

    for i, s in enumerate(signal):
        f.write(str(s))
        if i==len(signal)-1:
            f.write('};\n')
        else:
            f.write(',\n')

    f.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Write .h file given an audio file, for testing purposes.')
    parser.add_argument('filename_in', type=str, help='Path to audio file.')
    parser.add_argument('filename_out', type=str, help='Path to output .h file.')

    args = parser.parse_args()
    write_input_audio_h(args.filename_in, args.filename_out)