# Code generation

Install Ac6 SystemWorkbench.

Open SystemWorkbench `cd Ac6/SystemWorkbench` and then `./eclipse`.

Clone `onnx2sensortile`

Clone `onnxparser` inside `onnx2sensortile`, checkout branch `RPIfix`

In the `onnx2sensortile` directory

`export PATH=$PATH:./onnxparser`

`source create_project.sh -Q 0 new_app model.onnx`

Where model.onnx is the name of the model

Q = 0 because it already quantized

Disabling batch normalization folding (folding = 0) leads to less warnings.

## Test preprocessing 

Generate a .h file containing an array with the audio data for a sample with the script provided

`python3 write_input_audio_h.py /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/0ac15fe9_nohash_0.wav ./input_audio.h`

and then use the next commands to calculate the expected output from the sensortile

`export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/shared_data/"`
`python3 expected_preprocessing.py /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/0ac15fe9_nohash_0.wav pipelinemel3216staug.json --decimal_bits 3`

compare the results coming from the script with the ones coming from the sensortile.

## Test network

Generate a .h file containing a 2d array with the preprocessed input for the network with the following command
`export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/shared_data/"`
`python3 expected_preprocessing.py /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/0ac15fe9_nohash_0.wav melst3216aug.json -d 3 -e preprocessed_input.h`

## Test on SensorTile

* Clone `aloha.eu/kws_on_sensortile`
* Open System Workbench
* File import
* General existing projects
* Select kws_on_sensortile folder
* Check that you can Build project
* 
* Among the generated files in `sensortile_project/include` look for Conv0_weights.h, open it and remove all the even lines. Then replace the *2 in the first row with a *1 and check that the weights declaration is correctly closed with `};`. Save.
* Copy all the recently generated .h found in `sensortile_project/include` files into `kws_on_sensortile` in the directory `MLKWS/include`
* Open `cnnNet.h` and copy all the includes except `global.h`, `datalog_application.h`, and `input11562_16x32_RPI.h` into the file `aloha2.h` in place of the ones that where there before, from there keep just `nn.h` and `arm_math.h`
* 

