import numpy as np
import librosa
import random
import os
import argparse

def prepare_silence(noise_sample_folder, output_folder, number_of_samples, len_signal, predefined_srnoise, seed=None):
    noise_samples = []
    #fixed random seed for deterministic behavior
    random.seed(seed)
    # load noise samples paths
    for root, dirs, files in os.walk(noise_sample_folder, topdown=False):
        for f in files:
            if f.endswith('.wav'):
                path = root + "/" + f
                noise_filename = path.split('/')[-1].replace('.wav','')
                noise, srnoise = librosa.load(path)
                noise_samples.append({'filename': noise_filename, 'noise': noise})

    for i in range(number_of_samples):
        noise_item = random.choice(noise_samples)
        noise_filename = noise_item['filename']
        noise = noise_item['noise']
        if len_signal > len(noise):
            noise = librosa.util.pad_center(noise, len_signal)
        else:
            cut_here = random.randint(0, len(noise)-len_signal)
            noise = noise[cut_here:cut_here+len_signal]
        librosa.output.write_wav(output_folder+str(i)+noise_filename+'.wav', noise, predefined_srnoise)
        print(i)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare silence/background noise samples.')
    parser.add_argument('noise_input_folder', type=str, help='Folder containing the noise recordings to be divided in samples.')
    parser.add_argument('noise_output_folder', type=str, help='Folder to save the noise samples in output.')
    parser.add_argument('number_of_samples', type=int, help='Number of samples to generate.')
    parser.add_argument('-l', '--lenght', dest='sample_lenght', type=int, help='Lenght generated samples.', default=16000, required=False)
    parser.add_argument('-sr', '--signal_rate', dest='signal_rate', type=int, help='Signal rate output', default=16000, required=False)
    parser.add_argument('-s', '--seed', dest='random_seed', type=int, help='Integer random seed.', default=None, required=False)

    args = parser.parse_args()
    prepare_silence(args.noise_input_folder, args.noise_output_folder, args.number_of_samples, args.sample_lenght, args.signal_rate, args.random_seed)