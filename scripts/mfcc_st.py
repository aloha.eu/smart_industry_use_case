import librosa
import numpy as np
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import seaborn as sns
import matplotlib.pylab as plt
import math

filename = "mfcc0_test_aug.txt"

line_processed = 0

with open(filename) as f:
    content = f.readlines()

dataset = []

for line in content:
    [path, target] = line.rstrip().split(' ')

    line_processed = line_processed + 1

    dataset = dataset + [{'path': path.replace('/opt/www','/home/lrinelli/20200219_ALOHA'), 'target': target}]

    if(line_processed%100==0):
        print(line_processed)


def mfcc_st(im_file):
    img_size = [32, 16, 1]

    # Preprocessing for waw files to obtain a mfcc fingerprint.
    height = img_size[0]
    width = img_size[1]

    signal, sr = librosa.load(im_file, mono=False, sr=None)
    # center pad the signal with 0s to reach one second of length
    signal = librosa.util.pad_center(signal, sr)
    # height is used to determine the number of frames
    num_frames = height
    # frame_overlap ratio is (frame_len-frame_shift)/frame_len
    # it is an hyper parameter for this function
    frame_overlap_ratio = 0.5
    # frame_len and frame_shift are computed based on desired number of frames and desired overlap
    frame_len = 968 #int(len(signal)/(num_frames*(1-frame_overlap_ratio)+frame_overlap_ratio))
    frame_len_padded = 2**math.ceil(math.log2(frame_len))
    # frame_shift = math.ceil(frame_len*(1-frame_overlap_ratio))
    frame_shift = int((len(signal)-frame_len)/(num_frames-1))
    # the height of the images is used to determine of many mfcc coefficients to compute
    n_mfcc = width

    features = []

    for nf in range(num_frames):
        buffer=np.zeros(frame_len_padded)
        buffer[0: frame_len]=np.hanning(frame_len+1)[:frame_len]*signal[nf*frame_shift:nf*frame_shift+frame_len]
        fft=np.fft.rfft(buffer)
        D = (np.real(fft)**2+np.imag(fft)**2)
        D[0] = (np.real(fft[0])**2)
        D[512] = (np.imag(fft[512])**2)
        D=np.sqrt(D)
        mels_f = librosa.filters.mel(sr=sr, n_fft=frame_len_padded, n_mels=40, fmin=20, fmax=4000, norm=np.inf, htk=True)
        power = 1
        mels = mels_f.dot(D**power)
        mels[mels==0] = np.nextafter(np.float32(0), np.float32(1))
        lmels=np.log(mels)
        features.append(librosa.feature.mfcc(S=lmels, sr=sr, n_mfcc=n_mfcc, dct_type=2, norm='ortho'))

    features = np.array(features)
    # use shape with 3 dimensions
    return np.expand_dims(features, -1)

line_processed = 0
result_pre = []

for d in dataset:
    result_pre.append(mfcc_st(d['path']))
    line_processed = line_processed + 1
    if(line_processed%100==0):
        print(line_processed)

print("Perform min-max on whole dataset")
min_max_scaler = MinMaxScaler(copy=True)
standard_scaler = StandardScaler(copy=True)

dset_x = np.asarray(result_pre)

shape_t = dset_x.shape

print(shape_t)
with np.printoptions(precision=1, suppress=True):
    min_max_scaler.fit(dset_x.reshape((shape_t[0]*shape_t[1], shape_t[2]*shape_t[3])))
    print("data min",min_max_scaler.data_min_)
    ax = sns.heatmap(np.reshape(min_max_scaler.data_min_,(16,-1)), linewidth=0.5)
    plt.show()
    print("data max",min_max_scaler.data_max_)
    ax = sns.heatmap(np.reshape(min_max_scaler.data_max_,(16,-1)), linewidth=0.5)
    plt.show()
    standard_scaler.fit(dset_x.reshape((shape_t[0]*shape_t[1], shape_t[2]*shape_t[3])))
    print("mean",standard_scaler.mean_)
    ax = sns.heatmap(np.reshape(standard_scaler.mean_,(16,-1)), linewidth=0.5)
    plt.show()
    print("var",standard_scaler.var_)
    ax = sns.heatmap(np.reshape(standard_scaler.var_,(16,-1)), linewidth=0.5)
    plt.show()
    
f = open("feature_scaling_test.txt", "a")
f.write(str(min_max_scaler.data_min_))
f.write(str(min_max_scaler.data_max_))
f.close()
