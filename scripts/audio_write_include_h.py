import librosa
import numpy as np

filename = "/opt/www/shared_data/usecases/REPLY_KWS/yes/93cf902e_nohash_0.wav"
#filename = "/opt/www/shared_data/usecases/REPLY_KWS/no/0ba018fc_nohash_3.wav"


signal, sr = librosa.load(filename.replace('/opt/www','/home/lrinelli/20200219_ALOHA'), mono=False, sr=None, dtype=np.int16)

f = open("input_sample.h", "a")
f.write("int16_t input[16000]={")

for s in signal:
    f.write(str(s))
    f.write(',\n')

f.close()
