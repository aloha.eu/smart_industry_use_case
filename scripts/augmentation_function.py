import numpy as np
import librosa
#import librosa.display
#import matplotlib.pyplot as plt
import random
import os

top = "/home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/"
output_top = "/opt/www/shared_data/usecases/"
dataset_name = "REPLY_KWS"
output_file_name = "reply6_s1_aug.txt"
# len(dataset[target])*relative_augmentation => number of items with augmentation per class
relative_augmentation = 2
filename = top+"reply6_s1_split.txt"
noise_folder = top + "noise/"


def augmentation(signal_path, noise_dict, output_path, min_noise_coef=0.2, max_noise_coef=0.7):
    signal, sr = librosa.load(signal_path)
    noise = noise_dict['noise']

    if len(signal) > len(noise):
        noise = librosa.util.pad_center(noise, len(signal))
    elif len(signal) < len(noise):
        signal = librosa.util.pad_center(signal, len(noise))

    # plt.figure(figsize=(14, 5))
    # librosa.display.waveplot(signal, sr=sr)

    trimmed, trim_indexes = librosa.effects.trim(signal, top_db=30)
    max_time_shift = len(signal) - len(trimmed)
    time_shift = random.randint(0, max_time_shift)

    signal_time_shifted = np.pad(trimmed, (time_shift, max_time_shift - time_shift), 'constant', constant_values=0)

    # print(len(signal))
    # print(len(trimmed))

    # plt.figure(figsize=(14, 5))
    # librosa.display.waveplot(signal_time_shifted, sr=sr)

    noise_coef = random.uniform(min_noise_coef, max_noise_coef)

    # plt.figure(figsize=(14, 5))
    # librosa.display.waveplot(noise, sr=srnoise)

    signal_sum = noise_coef * (np.max(signal) / max(np.max(noise), 0.1)) * noise + signal_time_shifted

    # plt.figure(figsize=(14, 5))
    # librosa.display.waveplot(signal_sum, sr=sr)

    librosa.output.write_wav(output_path, signal_sum, sr)

    # plt.show()


def dataset_augmentation_file():
    line_processed = 0

    with open(filename) as f:
        content = f.readlines()

    dataset = []

    for line in content:
        [path, target] = line.rstrip().split(' ')

        line_processed = line_processed + 1

        dataset = dataset + [{'path': path, 'target': target}]

    noise_samples = []
    noise_n = 0

    # load noise samples paths
    for root, dirs, files in os.walk(noise_folder, topdown=False):
        for f in files:
            if f.endswith('.wav'):
                path = root + "/" + f
                noise_filename = path.split('/')[-1].replace('.wav','')
                noise, srnoise = librosa.load(path)
                noise_samples.append({'noise': noise})
                noise_n = noise_n + 1
                print(noise_n)
    

    to_be_augmented = random.choices(dataset, k=int(line_processed * relative_augmentation))

    line_processed = 0
    augmented_dataset = []

    for line in to_be_augmented:
        line_processed = line_processed + 1
        print(line_processed)

        s_path = line['path'].replace(output_top + dataset_name, top)
        n_dict = random.choice(noise_samples)
        o_path = s_path.replace(s_path.split('/')[-1], '') + s_path.split('/')[-1].split('.')[0] + "augmented" + str(line_processed) + ".wav"
        augmentation(s_path, n_dict, o_path)

        o_path = o_path.replace(top, output_top + dataset_name)
        augmented_dataset = augmented_dataset + [{'path': o_path, 'target': line['target']}]

    f = open(output_file_name, 'w')
    to_output_file = dataset + augmented_dataset

    for line in to_output_file:
        f.write(line['path'] + " " + line['target'] + "\n")
    f.close()


dataset_augmentation_file()
