import numpy as np
import seaborn as sns
import matplotlib.pylab as plt
import librosa
import math
from sklearn.preprocessing import MinMaxScaler

#filename = 'result_dct_fix'

filename = 'mfccst_sca_deb_res.txt'

with open(filename) as f:
    content = f.readlines()

result = []

for line in content:
    result.append(line.split('\t')[4].split(' ')[0])

result = np.array(result).astype(np.float)

f = open("preprocessed_input.h", "a")
f.write("uint8_t preprocessed_input[512]={")

for r in result:
    f.write(str(int(r)))
    f.write(',\n')

f.close()

result = np.reshape(result, (32, -1))

ax = sns.heatmap(result, linewidth=0.5)
plt.show()

def mfcc0(im_file):
    img_size = [16, 32, 1]

    # Preprocessing for waw files to obtain a mfcc fingerprint.
    height = img_size[0]
    width = img_size[1]

    signal, sr = librosa.load(im_file, mono=False, sr=None)
    print(signal[0])
    # center pad the signal with 0s to reach one second of length
    signal = librosa.util.pad_center(signal, sr)
    # width is used to determine the number of frames
    num_frames = width
    # frame_overlap ratio is (frame_len-frame_shift)/frame_len
    # it is an hyper parameter for this function
    frame_overlap_ratio = 0.5
    # frame_len and frame_shift are computed based on desired number of frames and desired overlap
    frame_len = int(len(signal)/(num_frames*(1-frame_overlap_ratio)+frame_overlap_ratio))
    print(frame_len)
    frame_shift = int((len(signal)-frame_len)/(num_frames-1))
    print(frame_shift)
    window_len = frame_len
    hop_length = frame_shift
    # the height of the images is used to determine of many mfcc coefficients to compute
    n_mfcc = height
    # the parameters used here are what I found to be most similar to the one used for Hello Edge
    features = librosa.feature.mfcc(signal, sr=sr, n_mfcc=n_mfcc,  n_fft=window_len, hop_length=hop_length, fmin=20, fmax=4000, n_mels=40, center=False, window='hann', dct_type=2, norm=None, htk=True, power=1.0)

    return features

def mfcc_new(im_file):
    img_size = [16, 32, 1]

    # Preprocessing for waw files to obtain a mfcc fingerprint.
    height = img_size[0]
    width = img_size[1]

    signal, sr = librosa.load(im_file, mono=False, sr=None)
    # center pad the signal with 0s to reach one second of length
    signal = librosa.util.pad_center(signal, sr)
    # width is used to determine the number of frames
    num_frames = width
    # frame_overlap ratio is (frame_len-frame_shift)/frame_len
    # it is an hyper parameter for this function
    frame_overlap_ratio = 0.5
    # frame_len and frame_shift are computed based on desired number of frames and desired overlap
    frame_len = int(len(signal)/(num_frames*(1-frame_overlap_ratio)+frame_overlap_ratio))
    print(frame_len)
    frame_shift = int((len(signal)-frame_len)/(num_frames-1))
    print(frame_shift)
    window_len = frame_len
    hop_length = frame_shift
    # the height of the images is used to determine of many mfcc coefficients to compute
    n_mfcc = height
    frame_len_padded = 2**math.ceil(math.log2(frame_len))

    fft=librosa.core.stft(signal, n_fft=frame_len_padded, win_length=frame_len, hop_length=frame_shift, center=False)
    # print(np.real(fft[:10]),np.imag(fft[:10]))
    # D=np.real(fft)**2+np.imag(fft)**2
    D = np.abs(fft)
    mels=librosa.feature.melspectrogram(S=D, sr=sr, power=1,  fmin=20, fmax=4000, n_mels=40, n_fft=frame_len_padded, win_length=frame_len, hop_length=frame_shift, center=False, htk=True)
    features=librosa.feature.mfcc(S=librosa.power_to_db(mels), sr=sr, n_mfcc=16, dct_type=2, norm='ortho')
    return features

def mfcc_for(im_file):
    img_size = [16, 32, 1]

    # Preprocessing for waw files to obtain a mfcc fingerprint.
    height = img_size[0]
    width = img_size[1]

    signal, sr = librosa.load(im_file, mono=False, sr=None)
    # center pad the signal with 0s to reach one second of length
    # signal = librosa.util.pad_center(signal, sr)
    # width is used to determine the number of frames
    num_frames = width
    # frame_overlap ratio is (frame_len-frame_shift)/frame_len
    # it is an hyper parameter for this function
    frame_overlap_ratio = 0.5
    # frame_len and frame_shift are computed based on desired number of frames and desired overlap
    frame_len = 968 #int(len(signal)/(num_frames*(1-frame_overlap_ratio)+frame_overlap_ratio))
    print(frame_len)
    frame_shift = int((len(signal)-frame_len)/(num_frames-1))
    print(frame_shift)
    window_len = frame_len
    hop_length = frame_shift
    # the height of the images is used to determine of many mfcc coefficients to compute
    n_mfcc = height
    frame_len_padded = 2**math.ceil(math.log2(frame_len))

    features = []

    for nf in range(num_frames):
        buffer=np.zeros(frame_len_padded)
        buffer[0: frame_len]=np.hanning(frame_len+1)[:frame_len]*signal[nf*frame_shift:nf*frame_shift+frame_len]
        #print(buffer[:30])
        fft=np.fft.rfft(buffer)
        #fft=librosa.core.stft(buffer, n_fft=frame_len_padded, win_length=frame_len, hop_length=frame_shift, center=False)
        #print(fft[:30])
        #print(fft[500:])
        D = (np.real(fft)**2+np.imag(fft)**2)
        D[0] = (np.real(fft[0])**2)
        D[512] = (np.imag(fft[512])**2)
        #print(D)
        #input()
        D=np.sqrt(D)
        #D = np.abs(fft)
        mels_f = librosa.filters.mel(sr=sr, n_fft=frame_len_padded, n_mels=40, fmin=20, fmax=4000, norm=np.inf, htk=True)
        #mels=librosa.feature.melspectrogram(S=D, sr=sr, power=1,  fmin=20, fmax=4000, n_mels=40, n_fft=frame_len_padded, win_length=frame_len, hop_length=frame_shift, center=False, htk=False)
        power = 1
        mels = mels_f.dot(D**power)
        #print(mels)
        lmels=np.log(mels)
        # print(lmels)
        #input()
        
        features.append(librosa.feature.mfcc(S=lmels, sr=sr, n_mfcc=16, dct_type=2, norm='ortho'))

    return features

#filename = "/opt/www/shared_data/usecases/REPLY_KWS/yes/0ac15fe9_nohash_0.wav"
#filename = "/opt/www/shared_data/usecases/REPLY_KWS/no/0ba018fc_nohash_3.wav"
#filename = "/opt/www/shared_data/usecases/REPLY_KWS/go/50d1c8e3_nohash_1.wav"
#filename = "/opt/www/shared_data/usecases/REPLY_KWS/stop/7fb8d703_nohash_4.wav"
#filename = "/opt/www/shared_data/usecases/REPLY_KWS/noise/84running_tapaugmented3697.wav"
filename = "/opt/www/shared_data/usecases/REPLY_KWS/right/39a12648_nohash_0.wav"

toolflow_res = mfcc_for(filename.replace('/opt/www','/home/lrinelli/20200219_ALOHA')) #, -32, 32)

toolflow_res = np.array(toolflow_res)

print(toolflow_res.shape)

# Feature scaling using min-max on whole dataset
min_max_scaler = MinMaxScaler(feature_range=(0, 255), copy=True)


min_max_scaler.data_min_ = np.array([-653.1933056,   -16.32959262,  -13.75402826,  -12.51952652,  -10.51510905, -6.6869989,    -6.38308796,   -4.71445984,   -4.27017508,   -5.37337953, -4.27339856,   -4.09903172,   -5.00980921,   -4.32528204,  -3.51895227,-3.35720724])
min_max_scaler.data_max_ = np.array([25.77084918, 20.07232629, 13.97542683, 12.62629092,  7.53502081, 7.64473243,6.05053552,  4.95373881,  4.11355196,  5.16546842,  3.33507885,  3.31829356,3.94662422,  2.63761765,  3.43963974,  3.36743616])

min_max_scaler.scale_ = 255 / (min_max_scaler.data_max_-min_max_scaler.data_min_)

min_max_scaler.min_ = -min_max_scaler.data_min_ * min_max_scaler.scale_

min_max_scaler.data_range_ = min_max_scaler.data_max_-min_max_scaler.data_min_

print("data min",min_max_scaler.data_min_)
print("data max",min_max_scaler.data_max_)

toolflow_res = min_max_scaler.transform(toolflow_res.reshape(32, 16))

f = open("preprocessed_input255right.h", "a")
f.write("uint8_t preprocessed_input[512]={")

for rs in toolflow_res:
    for r in rs:
        f.write(str(int(r)))
        f.write(',\n')

f.close()

print(toolflow_res)

ax = sns.heatmap(toolflow_res, linewidth=0.5)
plt.show()

diff = np.abs(toolflow_res-result)

print(np.mean(diff))

ax = sns.heatmap(diff, linewidth=0.5)
plt.show()
