import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
import nemo
from tqdm import tqdm

from onnx2pytorch.onnx2pytorch import onnxextract
import h5py
import logging
import json
import numpy as np

from te_metrics import *
from te_BatchIterator import *

args = {
    'onnx_path': './trained_onnx_model_mfccst.onnx',
    'input_shape': (1,32,16),
    'dataset': './test_reply_resized32x16.hdf5'
    }
    
n_classes = 6

regime = {'batch_size':100}

criterion = nn.CrossEntropyLoss()
marg = lambda y: y.max(dim=1)[1]

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
use_cuda = False

train_h5 = h5py.File(args['dataset'] + "train")
val_h5 = h5py.File(args['dataset'] + "train")
train_loader = BatchIterator(train_h5, np.arange(train_h5['y_data'].shape[0]), train_h5['X_data'].shape[1:], train_h5['y_data'].shape[-1], regime['batch_size'], 'classification') # FIXME task type
val_loader   = BatchIterator(val_h5,   np.arange(val_h5['y_data'].shape[0]),   val_h5['X_data'].shape[1:],   val_h5['y_data'].shape[-1],   regime['batch_size'], 'classification') # FIXME task type

# Model construction and quantization
logging.info('[RPI] Building PyTorch model from ONNX')
onnxextract(args['onnx_path'], class_elements=n_classes, path="onnxgennet")
import onnxgennet.Net as gennet
net = gennet.Net()

print(net)


conf_matrix = torch.zeros(n_classes, n_classes)

def confusion_matrix(preds, labels, conf_matrix):
    preds = torch.argmax(preds, 1)
    for p, t in zip(preds, labels):
        conf_matrix[t, p] += 1


shuffle = True
indeces = np.arange(train_h5['y_data'].shape[0])
if shuffle:
    np.random.shuffle(indeces)

def rpi_train(model, epoch, criterion=criterion, freeze_bn=False, normalize_range=True):

    train_loader = BatchIterator(train_h5, indeces, train_h5['X_data'].shape[1:], train_h5['y_data'].shape[-1], regime['batch_size'], 'classification') # FIXME task type

    model.train()
    if freeze_bn:
        if use_cuda:
            try:
                model_inner = model.module
            except AttributeError:
                model_inner = model
        else:
            model_inner = model
        model_inner.freeze_bn()
    train_loss = Metric('train_loss')
    train_accuracy = Metric('train_accuracy')

    with tqdm(total=train_loader.get_max_lim(),
            desc='Train Epoch     #{}'.format(epoch + 1)) as t:
        for batch_idx, x in enumerate(train_loader):
            ff = list(x)
            # shamelessly adapted from training engine TrainRunner.py
            # TODO: changed use of shape, should it be reverted? it was 1 2 3
            inputs  = torch.from_numpy(np.array([np.reshape(f[0], [train_h5['X_data'].shape[3], train_h5['X_data'].shape[1], train_h5['X_data'].shape[2]]) for f in ff]))
            targets = torch.from_numpy(np.array([f[1] for f in ff])) # FIXME -- task type

            inputs, targets = perm(inputs), marg(targets)

            # FIXME: this is suboptimal!!! change to something better...
            if normalize_range:
                inputs /= 255

            optimizer.zero_grad()
            output = model(inputs)
            loss = criterion(output, targets)
            loss.backward()
            optimizer.step()
            train_loss.update(loss)
            train_accuracy.update(accuracy(output, targets))
            t.set_postfix({'loss': train_loss.avg.item(),
                        'accuracy': 100. * train_accuracy.avg.item()})
            t.update(1)

    if tbx_writer:
        tbx_writer.add_scalar('train/loss', train_loss.avg, epoch)
        tbx_writer.add_scalar('train/accuracy', train_accuracy.avg, epoch)

    return train_loss.avg

def rpi_validate(model, epoch, val_loader=None, criterion=criterion, freeze_bn=False, normalize_range=True, eps_in=None):
    print(val_h5,   np.arange(val_h5['y_data'].shape[0]),   val_h5['X_data'].shape[1:],   val_h5['y_data'].shape[-1],   regime['batch_size'], 'classification')
    val_loader   = BatchIterator(val_h5,   np.arange(val_h5['y_data'].shape[0]),   val_h5['X_data'].shape[1:],   val_h5['y_data'].shape[-1],   regime['batch_size'], 'classification') # FIXME task type

    model.eval()
    if freeze_bn:
        if use_cuda:
            try:
                model_inner = model.module
            except AttributeError:
                model_inner = model
        else:
            model_inner = model
        model_inner.freeze_bn()
    val_loss = Metric('val_loss')
    val_accuracy = Metric('val_accuracy')

    with tqdm(total=val_loader.get_max_lim(),
            desc='Validate Epoch  #{}'.format(epoch + 1)) as t:
        with torch.no_grad():
            for batch_idx, x in enumerate(val_loader):
                ff = list(x)
                # shamelessly adapted from training engine TrainRunner.py
                # TODO: changed use of shape, should it be reverted? it was 1 2 3
                inputs  = torch.from_numpy(np.array([np.reshape(f[0], [val_h5['X_data'].shape[3], val_h5['X_data'].shape[1], val_h5['X_data'].shape[2]]) for f in ff]))
                targets = torch.from_numpy(np.array([f[1] for f in ff])) # FIXME -- task type
                
                inputs = inputs.float()
                targets = marg(targets)
                
                # FIXME: this is suboptimal!!! change to something better...
                if normalize_range:
                    inputs /= 255.

                if eps_in is not None:
                    inputs /= eps_in

                output = model(inputs)

                confusion_matrix(output, targets, conf_matrix)

                val_loss.update(criterion(output, targets))
                val_accuracy.update(accuracy(output, targets))
                del output, inputs, targets
                t.set_postfix({'loss': val_loss.avg.item(),
                            'accuracy': 100. * val_accuracy.avg.item()})
                t.update(1)
    
    return val_accuracy.avg

class Metric(object):
    def __init__(self, name):
        self.name = name
        self.sum = torch.tensor(0.)
        self.n = torch.tensor(0.)

    def update(self, val):
        self.sum += val.detach().cpu()
        self.n += 1

    @property
    def avg(self):
        return self.sum / self.n

def accuracy(output, target):
    # get the index of the max log-probability
    pred = output.max(1, keepdim=True)[1]
    return pred.eq(target.view_as(pred)).cpu().float().mean()

acc = rpi_validate(net, 0, val_loader, criterion)

print(conf_matrix)

import seaborn as sn
import matplotlib.pyplot as plt

plt.figure(figsize = (10,7))
sn.heatmap(conf_matrix, annot=True)
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()
