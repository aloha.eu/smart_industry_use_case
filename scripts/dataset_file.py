import os
import random
import argparse
import json


def prepare_dataset_file(top, output_file, aloha_folder, max_number_of_items_per_class, keywords_json, random_seed):
    i = 0
    with open(keywords_json, 'r') as file:  
        k_json = json.load(file)
    
    keywords = k_json['keywords']
    ignore_for_unknown = k_json['ignore_for_unknown']
    dataset = [[] for x in range(len(keywords))]

    random.seed(random_seed)

    for root, dirs, files in os.walk(top, topdown=False):
        if root.endswith('_'):
            print('skipping', root)
        else:
            for f in files:
                if f.endswith('.wav'):
                    dir_name = root.split('/')[-1]
                    if ('unknown' in keywords and dir_name not in ignore_for_unknown) or dir_name in keywords:
                        target = keywords.index(dir_name if dir_name in keywords else 'unknown')
                        path = root.replace(top, aloha_folder)+"/"+f
                        i = i+1
                        dataset[target].append(path)

    t = 0
    a = 0
    f = open(output_file, 'w')

    for d in dataset:
        if len(d) > max_number_of_items_per_class:
            d = random.sample(d, max_number_of_items_per_class)
        for l in d:
            f.write(l+" "+str(t)+"\n")
            a = a + 1
        t = t + 1

    f.close()
    print(i, 'files candidate')
    print(a, 'files added to dataset')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare dataset file.')
    parser.add_argument('dataset_folder', type=str, help='Folder containing the folders containing the dataset samples.')
    parser.add_argument('output_file', type=str, help='Output dataset file.')
    parser.add_argument('aloha_folder', type=str, help='Dataset folder path inside ALOHA toolflow.')
    parser.add_argument('number_of_samples', type=int, help='Number of samples to add per class.')
    parser.add_argument('keywords_json', type=str, help='Path to json file containing selected keywords')
    parser.add_argument('-s', '--seed', dest='random_seed', type=int, help='Integer random seed.', default=None, required=False)

    args = parser.parse_args()
    prepare_dataset_file(args.dataset_folder, args.output_file, args.aloha_folder, args.number_of_samples, args.keywords_json, args.random_seed)