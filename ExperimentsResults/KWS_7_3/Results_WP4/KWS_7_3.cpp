
//This code is auto_generated.//

//This file must be embedded in the host application that will provvide the data to be processed.



/* ################## Net description ###################
Source file: /opt/data/experiments/prj_5defb500707e58436a372e74/alg_5defc02b8a3e13fa0b876fe0/onnx_storage/o56_1x32x32_7/2019-12-10_15-56-29/onnx_model.onnx

Layer         IF    ih    iw    OF    oh    ow
Conv0          1    32    32    64    32    32
Relu0         64    32    32    64    32    32
Conv1         64    32    32    64    32    32
Relu1         64    32    32    64    32    32
MaxPool0      64    32    32    64    16    16
Conv2         64    16    16   128    16    16
Relu2        128    16    16   128    16    16
Conv3        128    16    16   128    16    16
Relu3        128    16    16   128    16    16
MaxPool1     128    16    16   128     8     8
Flatten0     128     8     8   128     8     8
Gemm0       8192     8     8   200     1     1
Gemm1        200     1     1     7     1     1
#########################################################*/

#include "KWS_7_3.h" 
#include "assert.h"
#define ELEM_PER_WS 32

// global variables

#if !(_RELEASE_)
     #define _rprintf_(...) printf(__VA_ARGS__)
     #define spatconv_forward_hw(...) spatconv_forward_hw_sync(__VA_ARGS__)
     #ifdef _DEBUG_
         #define _dprintf_(...) printf(__VA_ARGS__)
         #define _dprint_data_(...) print_data(__VA_ARGS__)
        #else
         #define _dprintf_(...) ;
         #define _dprint_data_(...) ;
        #endif
#else
       #define _rprintf_(...) ;
       #define _dprintf_(...) ;
       #define _dprint_data_(...) ;
#endif


// global variables
DATA* wPointer;
DATA* aPointer;

SPATCONV Conv0_param; 
RELU Relu0_param; 
SPATCONV Conv1_param; 
RELU Relu1_param; 
MAXPOOL MaxPool0_param; 
SPATCONV Conv2_param; 
RELU Relu2_param; 
SPATCONV Conv3_param; 
RELU Relu3_param; 
MAXPOOL MaxPool1_param; 
LINEAR Gemm0_param;

LINEAR Gemm1_param;



void cnnMainInit(VARNAME load_data_dir)
{
        #define _NCOL_ 4
        #define _NROW_ 4
        int minIF=0;
        int wsize=0;
        int minOF=0;
        int IG=0, OG=0;
VARNAME filename;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv0_param = spatconv_create();
 Conv0_param->pout =64;
 Conv0_param->pin = 1;

 Conv0_param->kern_s[0] = 64;
 Conv0_param->kern_s[1] = 1;
 Conv0_param->kern_s[2] = 3;
 Conv0_param->kern_s[3] = 3;

 Conv0_param->in_s[0] = 1;
 Conv0_param->in_s[1] = 32;
 Conv0_param->in_s[2] = 32;

 Conv0_param->out_s[0] = 64;
 Conv0_param->out_s[1] = 32;
 Conv0_param->out_s[2] = 32;

 Conv0_param->qf = 8;
 Conv0_param->activate = 0;
 Conv0_param->precision8 = 0;

Conv0_param->pad[0] = 1;
Conv0_param->pad[1] = 1;

Conv0_param->stride[0] = 1;
Conv0_param->stride[1] = 1;

Conv0_param->dil[0] = 1;
Conv0_param->dil[1] = 1;

 if (Conv0_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv0_param->pin/minIF;
 if (Conv0_param->pin%minIF)
     IG++;  

 OG= Conv0_param->pout/minOF;
 if (Conv0_param->pout%minOF)
     OG++;

 Conv0_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu0_param = relu_create();
Relu0_param->size[0] = 64;
Relu0_param->size[1] = 32;
Relu0_param->size[2] = 32;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv1_param = spatconv_create();
 Conv1_param->pout =64;
 Conv1_param->pin = 64;

 Conv1_param->kern_s[0] = 64;
 Conv1_param->kern_s[1] = 64;
 Conv1_param->kern_s[2] = 3;
 Conv1_param->kern_s[3] = 3;

 Conv1_param->in_s[0] = 64;
 Conv1_param->in_s[1] = 32;
 Conv1_param->in_s[2] = 32;

 Conv1_param->out_s[0] = 64;
 Conv1_param->out_s[1] = 32;
 Conv1_param->out_s[2] = 32;

 Conv1_param->qf = 8;
 Conv1_param->activate = 0;
 Conv1_param->precision8 = 0;

Conv1_param->pad[0] = 1;
Conv1_param->pad[1] = 1;

Conv1_param->stride[0] = 1;
Conv1_param->stride[1] = 1;

Conv1_param->dil[0] = 1;
Conv1_param->dil[1] = 1;

 if (Conv1_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv1_param->pin/minIF;
 if (Conv1_param->pin%minIF)
     IG++;  

 OG= Conv1_param->pout/minOF;
 if (Conv1_param->pout%minOF)
     OG++;

 Conv1_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu1_param = relu_create();
Relu1_param->size[0] = 64;
Relu1_param->size[1] = 32;
Relu1_param->size[2] = 32;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool0_param = maxpool_create();
MaxPool0_param->in_s[0] = 64;
MaxPool0_param->in_s[1] = 32;
MaxPool0_param->in_s[2] = 32;
MaxPool0_param->out_s[0] = 64;
MaxPool0_param->out_s[1] = 16;
MaxPool0_param->out_s[2] = 16;
MaxPool0_param->kern_s[0] = 2;
MaxPool0_param->kern_s[1] = 2;
MaxPool0_param->stride[0] = 2;
MaxPool0_param->stride[1] = 2;
MaxPool0_param->pad[0] = 0;
MaxPool0_param->pad[1] = 0;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv2_param = spatconv_create();
 Conv2_param->pout =128;
 Conv2_param->pin = 64;

 Conv2_param->kern_s[0] = 128;
 Conv2_param->kern_s[1] = 64;
 Conv2_param->kern_s[2] = 3;
 Conv2_param->kern_s[3] = 3;

 Conv2_param->in_s[0] = 64;
 Conv2_param->in_s[1] = 16;
 Conv2_param->in_s[2] = 16;

 Conv2_param->out_s[0] = 128;
 Conv2_param->out_s[1] = 16;
 Conv2_param->out_s[2] = 16;

 Conv2_param->qf = 8;
 Conv2_param->activate = 0;
 Conv2_param->precision8 = 0;

Conv2_param->pad[0] = 1;
Conv2_param->pad[1] = 1;

Conv2_param->stride[0] = 1;
Conv2_param->stride[1] = 1;

Conv2_param->dil[0] = 1;
Conv2_param->dil[1] = 1;

 if (Conv2_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv2_param->pin/minIF;
 if (Conv2_param->pin%minIF)
     IG++;  

 OG= Conv2_param->pout/minOF;
 if (Conv2_param->pout%minOF)
     OG++;

 Conv2_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu2_param = relu_create();
Relu2_param->size[0] = 128;
Relu2_param->size[1] = 16;
Relu2_param->size[2] = 16;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv3_param = spatconv_create();
 Conv3_param->pout =128;
 Conv3_param->pin = 128;

 Conv3_param->kern_s[0] = 128;
 Conv3_param->kern_s[1] = 128;
 Conv3_param->kern_s[2] = 3;
 Conv3_param->kern_s[3] = 3;

 Conv3_param->in_s[0] = 128;
 Conv3_param->in_s[1] = 16;
 Conv3_param->in_s[2] = 16;

 Conv3_param->out_s[0] = 128;
 Conv3_param->out_s[1] = 16;
 Conv3_param->out_s[2] = 16;

 Conv3_param->qf = 8;
 Conv3_param->activate = 0;
 Conv3_param->precision8 = 0;

Conv3_param->pad[0] = 1;
Conv3_param->pad[1] = 1;

Conv3_param->stride[0] = 1;
Conv3_param->stride[1] = 1;

Conv3_param->dil[0] = 1;
Conv3_param->dil[1] = 1;

 if (Conv3_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv3_param->pin/minIF;
 if (Conv3_param->pin%minIF)
     IG++;  

 OG= Conv3_param->pout/minOF;
 if (Conv3_param->pout%minOF)
     OG++;

 Conv3_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu3_param = relu_create();
Relu3_param->size[0] = 128;
Relu3_param->size[1] = 16;
Relu3_param->size[2] = 16;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool1_param = maxpool_create();
MaxPool1_param->in_s[0] = 128;
MaxPool1_param->in_s[1] = 16;
MaxPool1_param->in_s[2] = 16;
MaxPool1_param->out_s[0] = 128;
MaxPool1_param->out_s[1] = 8;
MaxPool1_param->out_s[2] = 8;
MaxPool1_param->kern_s[0] = 2;
MaxPool1_param->kern_s[1] = 2;
MaxPool1_param->stride[0] = 2;
MaxPool1_param->stride[1] = 2;
MaxPool1_param->pad[0] = 0;
MaxPool1_param->pad[1] = 0;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm0_param = linear_create();
Gemm0_param->in_s = 8192;
Gemm0_param->out_s = 200;
Gemm0_param->in_s = 8192;
 Gemm0_param->qf = 8;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm1_param = linear_create();
Gemm1_param->out_s = 7;
Gemm1_param->in_s = 200;
 Gemm1_param->qf = 8;


// ##################################################################
// ######################## WEIGHTS LOADING #######################
// ################################################################

//	Only convolutional layers and FC or GEMM layers have weights and biases
//	Convolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the soc. 
//	The FC and GEMM weights and biases may be stored in any place 

sprintf(filename, "%s/Conv0_weights.bin", load_data_dir);
Conv0_param -> kernel = (DATA*)malloc(576 * sizeof(DATA));
load_fixed(filename, 576, Conv0_param -> kernel);
sprintf(filename, "%s/Conv0_biases.bin", load_data_dir);
Conv0_param -> bias = (DATA*)malloc(64 * sizeof(DATA));
load_fixed(filename, 64, Conv0_param -> bias);


sprintf(filename, "%s/Conv1_weights.bin", load_data_dir);
Conv1_param -> kernel = (DATA*)malloc(36864 * sizeof(DATA));
load_fixed(filename, 36864, Conv1_param -> kernel);
sprintf(filename, "%s/Conv1_biases.bin", load_data_dir);
Conv1_param -> bias = (DATA*)malloc(64 * sizeof(DATA));
load_fixed(filename, 64, Conv1_param -> bias);



sprintf(filename, "%s/Conv2_weights.bin", load_data_dir);
Conv2_param -> kernel = (DATA*)malloc(73728 * sizeof(DATA));
load_fixed(filename, 73728, Conv2_param -> kernel);
sprintf(filename, "%s/Conv2_biases.bin", load_data_dir);
Conv2_param -> bias = (DATA*)malloc(128 * sizeof(DATA));
load_fixed(filename, 128, Conv2_param -> bias);


sprintf(filename, "%s/Conv3_weights.bin", load_data_dir);
Conv3_param -> kernel = (DATA*)malloc(147456 * sizeof(DATA));
load_fixed(filename, 147456, Conv3_param -> kernel);
sprintf(filename, "%s/Conv3_biases.bin", load_data_dir);
Conv3_param -> bias = (DATA*)malloc(128 * sizeof(DATA));
load_fixed(filename, 128, Conv3_param -> bias);




Gemm0_param->weights = (DATA*)malloc(1638400 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_weights.bin", load_data_dir);
load_fixed(filename, 1638400, Gemm0_param->weights);
Gemm0_param->bias = (DATA*)malloc(200 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_biases.bin", load_data_dir);
load_fixed(filename,200,Gemm0_param->bias);

Gemm1_param->weights = (DATA*)malloc(1400 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_weights.bin", load_data_dir);
load_fixed(filename, 1400, Gemm1_param->weights);
Gemm1_param->bias = (DATA*)malloc(7 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_biases.bin", load_data_dir);
load_fixed(filename,7,Gemm1_param->bias);



// ########################################################
// ######################## MEMORY ALLOCATION #######################
// #######################################################

//	Each layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.
//	We assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.
//	By editing this you can customize the memory utilization

Conv0_param->input = (DATA*)malloc(1024*sizeof(DATA));
Conv0_param->output = (DATA*)malloc(65536*sizeof(DATA));
Relu0_param->output = (DATA*)malloc(65536*sizeof(DATA));
Conv1_param->output = (DATA*)malloc(65536*sizeof(DATA));
Relu1_param->output = (DATA*)malloc(65536*sizeof(DATA));
MaxPool0_param->output = (DATA*)malloc(16384*sizeof(DATA));
Conv2_param->output = (DATA*)malloc(32768*sizeof(DATA));
Relu2_param->output = (DATA*)malloc(32768*sizeof(DATA));
Conv3_param->output = (DATA*)malloc(32768*sizeof(DATA));
Relu3_param->output = (DATA*)malloc(32768*sizeof(DATA));
MaxPool1_param->output = (DATA*)malloc(8192*sizeof(DATA));
Gemm0_param->output = (DATA*)malloc(200*sizeof(DATA));
Gemm1_param->output = (DATA*)malloc(7*sizeof(DATA));



// ########################################################
// ######################## WIRING #######################
// #######################################################

//	Each layer has an input and an output pointer. The connection between two layers is defined here


Relu0_param->input = Conv0_param->output;
Conv1_param->input = Relu0_param->output;
Relu1_param->input = Conv1_param->output;
MaxPool0_param->input = Relu1_param->output;
Conv2_param->input = MaxPool0_param->output;
Relu2_param->input = Conv2_param->output;
Conv3_param->input = Relu2_param->output;
Relu3_param->input = Conv3_param->output;
MaxPool1_param->input = Relu3_param->output;
DATA* Flatten0_dummy = MaxPool1_param->output;
Gemm0_param->input = Flatten0_dummy;
Gemm1_param->input = Gemm0_param->output;



}

//#########################################################################################################################################
//#########################################################################################################################################


void cnnMain(DATA* image, DATA* results)
{
SIZE input_size[] = {1,32,32}; 
SIZE batch_join_dim = 1 * 32 * 32;
for (int j=0;j<batch_join_dim;j++)
  Conv0_param->input[j]=image[j];



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv0_job_id =spatconv_forward_wrap (Conv0_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu0_param);



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv1_job_id =spatconv_forward_wrap (Conv1_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu1_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool0_param);



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv2_job_id =spatconv_forward_wrap (Conv2_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu2_param);



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv3_job_id =spatconv_forward_wrap (Conv3_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu3_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool1_param);

//flatten 

//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm0_param);


//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm1_param);



for(unsigned int i = 0; i < 7; i++){
	results[i] = Gemm1_param->output[i];
	_dprintf_("[%d] %d \n", i, Gemm1_param->output[i]);
}
//exit(0); 
}

 

void getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){
// This function is executed for each file in the images folder.
// After you load the content of the file you can perform whatever preprocessing you want

  //source_forward(image_path, image_pixels, inSz, _QF_); // preprocesses JPG images 
  load_fixed(image_path,1024,image_pixels);// loads binary files

}


int resultsProcessing(DATA* results, int size){
//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)
//The simplest classifier is a maximum search for the results which returns the index value of the maximum

 char *labels[7]={"label_0", "label_1", "label_2", "label_3", "label_4", "label_5", "label_6"};

  int top0=0;
  DATA topval=results[0];
  for (int i =1;i<size;i++){
    if (results[i]>topval){
      top0=i;
      topval=results[i];
    }  
  }
  printf("TOP 0: [%d] %s\n", top0, labels[top0]);
  return top0;
}
