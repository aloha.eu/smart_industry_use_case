# Report on the Classification Results
ALOHA Toolflow is assessed through various classification tests, considering different inputs and configurations as described in the table and the results are reported below.
The evaluation will continue during Y3 of the project and the contents of this report will be updated accordingly.
  
## Part I: Configuration
In order to have meaningful comparison, a set of common settings were set equally; The settings related to the “Constraints” and “Architecture Description” and “Images” are not modified, but there were some modifications in the other parts which are explained as follows:
  - __Mode Selection:__  
	In all considered cases the settings of the “Module Selection” was set as below:  	  
	![](./Images/1.png)
	  
  - __Toolflow Settings:__  
	Regarding the ToolFlow settings, in all the cases, “KWS” is set for the “Use Cases” and “Dataset Name” is selected as “CUSTOM”.
	On the other hand, Dataset file is changed every time based on the classification job we are going to conduct. The “Split on train/validation” is set as 0.7, but the “Number of classes” is again set based on the requirements of the user.
	Following is an example of selected settings for the “Toolflow Settings” part:  	  
	![](./Images/2.png)
	  
  - __Learning Settings__  
	For this part, “Algorithms folder” is set every time based on the needs, the other parts are left as they are, except the “Batch size” which is set equal to 16 for all the cases.  
	![](./Images/3.png)
  

## Part II: Saved Figures and Collected files
For each experiment, a seperate folder is created with the following name format:
```bash
(Usecase)_(NumberOfClasses)_(NumberOfAlgorithmsUsed)
```

For each step of the process, important information and the output data has been saved in a specific folder with the following structure:
   1. __Input_parameters__  
   Includes the screenshot of the configurations set for the job and the input ONNX files used for conducting the classification algorithm.
   2. __Results_WP2__  
   Includes the screenshot of the Results, Screenshot of the Tensorboard page, SVG files of the plots in the Tensorboard page, and output trained ONNX files.
   3. __Results_WP3__  
   Includes screenshot of the results, Visualization of the used ONNX and the SESAME result.
   4. __Results_WP4__  
   Includes the generated CPP code.
  
## Part III: Review on the Summary of the Results
The following table indicates a summary of the classification tasks done, the modes selected for each classification task and the accuracy.
As stated above these are preliminary results obtained with the current version of the Toolflow and will be updated as the integration progresses.

| **Number of Classes** | **Used Keywords** | **Training** | **Perf Evaluation** | **Accuracy** | **Link to the Results** |
| :---: | :---: | :---: | :---: | :---: | :---: |
| 2 | down, up | &#9745; | &#9745; | 87.86% | <a href="./KWS_2_3/">Click</a> |
| 4 | down, up, yes, no | &#9745; | &#9745; | 85.82% | <a href="./KWS_4_3/">Click</a> |
| 5 | backward, forward, go, learn, stop | &#9745; | &#9745; | 84.14% | <a href="./KWS_5_3/">Click</a> |
| 7 | backward, forward, go, learn, stop, silence, unknown | &#9745; | &#9745; | 63.84% | <a href="./KWS_7_3/">Click</a> |
| 10 | down, go, left, no, off, on, right, stop, up, yes | &#9745; | &#9745; | 84.29% | <a href="./KWS_10_3/">Click</a> |
| 12 | down, go, left, no, off, on, right, stop, up, yes, silence, unknown | &#9745; |&#9745; | 53.24% | <a href="./KWS_12_3/">Click</a> | 
