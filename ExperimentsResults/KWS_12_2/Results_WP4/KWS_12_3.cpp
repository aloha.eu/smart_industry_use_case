
//This code is auto_generated.//

//This file must be embedded in the host application that will provvide the data to be processed.



/* ################## Net description ###################
Source file: /opt/data/experiments/prj_5df240df786b1704a4168fe4/alg_5df2547d6ee5162386bc0509/onnx_storage/o12_1x32x32_12/2019-12-12_14-53-54/onnx_model.onnx

Layer         IF    ih    iw    OF    oh    ow
Conv0          1    32    32    32    32    32
Relu0         32    32    32    32    32    32
Conv1         32    32    32    32    32    32
Relu1         32    32    32    32    32    32
MaxPool0      32    32    32    32    16    16
Flatten0      32    16    16    32    16    16
Gemm0       8192    16    16   200     1     1
Gemm1        200     1     1    12     1     1
#########################################################*/

#include "KWS_12_3.h" 
#include "assert.h"
#define ELEM_PER_WS 32

// global variables

#if !(_RELEASE_)
     #define _rprintf_(...) printf(__VA_ARGS__)
     #define spatconv_forward_hw(...) spatconv_forward_hw_sync(__VA_ARGS__)
     #ifdef _DEBUG_
         #define _dprintf_(...) printf(__VA_ARGS__)
         #define _dprint_data_(...) print_data(__VA_ARGS__)
        #else
         #define _dprintf_(...) ;
         #define _dprint_data_(...) ;
        #endif
#else
       #define _rprintf_(...) ;
       #define _dprintf_(...) ;
       #define _dprint_data_(...) ;
#endif


// global variables
DATA* wPointer;
DATA* aPointer;

SPATCONV Conv0_param; 
RELU Relu0_param; 
SPATCONV Conv1_param; 
RELU Relu1_param; 
MAXPOOL MaxPool0_param; 
LINEAR Gemm0_param;

LINEAR Gemm1_param;



void cnnMainInit(VARNAME load_data_dir)
{
        #define _NCOL_ 4
        #define _NROW_ 4
        int minIF=0;
        int wsize=0;
        int minOF=0;
        int IG=0, OG=0;
VARNAME filename;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv0_param = spatconv_create();
 Conv0_param->pout =32;
 Conv0_param->pin = 1;

 Conv0_param->kern_s[0] = 32;
 Conv0_param->kern_s[1] = 1;
 Conv0_param->kern_s[2] = 3;
 Conv0_param->kern_s[3] = 3;

 Conv0_param->in_s[0] = 1;
 Conv0_param->in_s[1] = 32;
 Conv0_param->in_s[2] = 32;

 Conv0_param->out_s[0] = 32;
 Conv0_param->out_s[1] = 32;
 Conv0_param->out_s[2] = 32;

 Conv0_param->qf = 8;
 Conv0_param->activate = 0;
 Conv0_param->precision8 = 0;

Conv0_param->pad[0] = 1;
Conv0_param->pad[1] = 1;

Conv0_param->stride[0] = 1;
Conv0_param->stride[1] = 1;

Conv0_param->dil[0] = 1;
Conv0_param->dil[1] = 1;

 if (Conv0_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv0_param->pin/minIF;
 if (Conv0_param->pin%minIF)
     IG++;  

 OG= Conv0_param->pout/minOF;
 if (Conv0_param->pout%minOF)
     OG++;

 Conv0_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu0_param = relu_create();
Relu0_param->size[0] = 32;
Relu0_param->size[1] = 32;
Relu0_param->size[2] = 32;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv1_param = spatconv_create();
 Conv1_param->pout =32;
 Conv1_param->pin = 32;

 Conv1_param->kern_s[0] = 32;
 Conv1_param->kern_s[1] = 32;
 Conv1_param->kern_s[2] = 3;
 Conv1_param->kern_s[3] = 3;

 Conv1_param->in_s[0] = 32;
 Conv1_param->in_s[1] = 32;
 Conv1_param->in_s[2] = 32;

 Conv1_param->out_s[0] = 32;
 Conv1_param->out_s[1] = 32;
 Conv1_param->out_s[2] = 32;

 Conv1_param->qf = 8;
 Conv1_param->activate = 0;
 Conv1_param->precision8 = 0;

Conv1_param->pad[0] = 1;
Conv1_param->pad[1] = 1;

Conv1_param->stride[0] = 1;
Conv1_param->stride[1] = 1;

Conv1_param->dil[0] = 1;
Conv1_param->dil[1] = 1;

 if (Conv1_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv1_param->pin/minIF;
 if (Conv1_param->pin%minIF)
     IG++;  

 OG= Conv1_param->pout/minOF;
 if (Conv1_param->pout%minOF)
     OG++;

 Conv1_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu1_param = relu_create();
Relu1_param->size[0] = 32;
Relu1_param->size[1] = 32;
Relu1_param->size[2] = 32;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool0_param = maxpool_create();
MaxPool0_param->in_s[0] = 32;
MaxPool0_param->in_s[1] = 32;
MaxPool0_param->in_s[2] = 32;
MaxPool0_param->out_s[0] = 32;
MaxPool0_param->out_s[1] = 16;
MaxPool0_param->out_s[2] = 16;
MaxPool0_param->kern_s[0] = 2;
MaxPool0_param->kern_s[1] = 2;
MaxPool0_param->stride[0] = 2;
MaxPool0_param->stride[1] = 2;
MaxPool0_param->pad[0] = 0;
MaxPool0_param->pad[1] = 0;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm0_param = linear_create();
Gemm0_param->in_s = 8192;
Gemm0_param->out_s = 200;
Gemm0_param->in_s = 8192;
 Gemm0_param->qf = 8;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm1_param = linear_create();
Gemm1_param->out_s = 12;
Gemm1_param->in_s = 200;
 Gemm1_param->qf = 8;


// ##################################################################
// ######################## WEIGHTS LOADING #######################
// ################################################################

//	Only convolutional layers and FC or GEMM layers have weights and biases
//	Convolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the soc. 
//	The FC and GEMM weights and biases may be stored in any place 

sprintf(filename, "%s/Conv0_weights.bin", load_data_dir);
Conv0_param -> kernel = (DATA*)malloc(288 * sizeof(DATA));
load_fixed(filename, 288, Conv0_param -> kernel);
sprintf(filename, "%s/Conv0_biases.bin", load_data_dir);
Conv0_param -> bias = (DATA*)malloc(32 * sizeof(DATA));
load_fixed(filename, 32, Conv0_param -> bias);


sprintf(filename, "%s/Conv1_weights.bin", load_data_dir);
Conv1_param -> kernel = (DATA*)malloc(9216 * sizeof(DATA));
load_fixed(filename, 9216, Conv1_param -> kernel);
sprintf(filename, "%s/Conv1_biases.bin", load_data_dir);
Conv1_param -> bias = (DATA*)malloc(32 * sizeof(DATA));
load_fixed(filename, 32, Conv1_param -> bias);




Gemm0_param->weights = (DATA*)malloc(1638400 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_weights.bin", load_data_dir);
load_fixed(filename, 1638400, Gemm0_param->weights);
Gemm0_param->bias = (DATA*)malloc(200 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_biases.bin", load_data_dir);
load_fixed(filename,200,Gemm0_param->bias);

Gemm1_param->weights = (DATA*)malloc(2400 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_weights.bin", load_data_dir);
load_fixed(filename, 2400, Gemm1_param->weights);
Gemm1_param->bias = (DATA*)malloc(12 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_biases.bin", load_data_dir);
load_fixed(filename,12,Gemm1_param->bias);



// ########################################################
// ######################## MEMORY ALLOCATION #######################
// #######################################################

//	Each layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.
//	We assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.
//	By editing this you can customize the memory utilization

Conv0_param->input = (DATA*)malloc(1024*sizeof(DATA));
Conv0_param->output = (DATA*)malloc(32768*sizeof(DATA));
Relu0_param->output = (DATA*)malloc(32768*sizeof(DATA));
Conv1_param->output = (DATA*)malloc(32768*sizeof(DATA));
Relu1_param->output = (DATA*)malloc(32768*sizeof(DATA));
MaxPool0_param->output = (DATA*)malloc(8192*sizeof(DATA));
Gemm0_param->output = (DATA*)malloc(200*sizeof(DATA));
Gemm1_param->output = (DATA*)malloc(12*sizeof(DATA));



// ########################################################
// ######################## WIRING #######################
// #######################################################

//	Each layer has an input and an output pointer. The connection between two layers is defined here


Relu0_param->input = Conv0_param->output;
Conv1_param->input = Relu0_param->output;
Relu1_param->input = Conv1_param->output;
MaxPool0_param->input = Relu1_param->output;
DATA* Flatten0_dummy = MaxPool0_param->output;
Gemm0_param->input = Flatten0_dummy;
Gemm1_param->input = Gemm0_param->output;



}

//#########################################################################################################################################
//#########################################################################################################################################


void cnnMain(DATA* image, DATA* results)
{
SIZE input_size[] = {1,32,32}; 
SIZE batch_join_dim = 1 * 32 * 32;
for (int j=0;j<batch_join_dim;j++)
  Conv0_param->input[j]=image[j];



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv0_job_id =spatconv_forward_wrap (Conv0_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu0_param);



//################################################    Conv layer ###############################################
//kernel 3x3
int Conv1_job_id =spatconv_forward_wrap (Conv1_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu1_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool0_param);

//flatten 

//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm0_param);


//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm1_param);



for(unsigned int i = 0; i < 12; i++){
	results[i] = Gemm1_param->output[i];
	_dprintf_("[%d] %d \n", i, Gemm1_param->output[i]);
}
//exit(0); 
}

 

void getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){
// This function is executed for each file in the images folder.
// After you load the content of the file you can perform whatever preprocessing you want

  //source_forward(image_path, image_pixels, inSz, _QF_); // preprocesses JPG images 
  load_fixed(image_path,1024,image_pixels);// loads binary files

}


int resultsProcessing(DATA* results, int size){
//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)
//The simplest classifier is a maximum search for the results which returns the index value of the maximum

 char *labels[12]={"label_0", "label_1", "label_2", "label_3", "label_4", "label_5", "label_6", "label_7", "label_8", "label_9", "label_10", "label_11"};

  int top0=0;
  DATA topval=results[0];
  for (int i =1;i<size;i++){
    if (results[i]>topval){
      top0=i;
      topval=results[i];
    }  
  }
  printf("TOP 0: [%d] %s\n", top0, labels[top0]);
  return top0;
}
