
//This code is auto_generated.//

//This file must be embedded in the host application that will provvide the data to be processed.



/* ################## Net description ###################
Source file: /opt/data/experiments/prj_5df0b249707e58436a372e79/alg_5df0cd941859ecdd76d10a6b/onnx_storage/o28_1x32x32_10/2019-12-11_11-06-01/onnx_model.onnx

Layer         IF    ih    iw    OF    oh    ow
Conv0          1    32    32    32    32    32
Relu0         32    32    32    32    32    32
MaxPool0      32    32    32    32    16    16
Conv1         32    16    16    64    16    16
Relu1         64    16    16    64    16    16
MaxPool1      64    16    16    64     8     8
Conv2         64     8     8   128     8     8
Relu2        128     8     8   128     8     8
MaxPool2     128     8     8   128     4     4
Flatten0     128     4     4   128     4     4
Gemm0       2048     4     4    50     1     1
Gemm1         50     1     1    10     1     1
#########################################################*/

#include "KWS_10_3.h" 
#include "assert.h"
#define ELEM_PER_WS 32

// global variables

#if !(_RELEASE_)
     #define _rprintf_(...) printf(__VA_ARGS__)
     #define spatconv_forward_hw(...) spatconv_forward_hw_sync(__VA_ARGS__)
     #ifdef _DEBUG_
         #define _dprintf_(...) printf(__VA_ARGS__)
         #define _dprint_data_(...) print_data(__VA_ARGS__)
        #else
         #define _dprintf_(...) ;
         #define _dprint_data_(...) ;
        #endif
#else
       #define _rprintf_(...) ;
       #define _dprintf_(...) ;
       #define _dprint_data_(...) ;
#endif


// global variables
DATA* wPointer;
DATA* aPointer;

SPATCONV Conv0_param; 
RELU Relu0_param; 
MAXPOOL MaxPool0_param; 
SPATCONV Conv1_param; 
RELU Relu1_param; 
MAXPOOL MaxPool1_param; 
SPATCONV Conv2_param; 
RELU Relu2_param; 
MAXPOOL MaxPool2_param; 
LINEAR Gemm0_param;

LINEAR Gemm1_param;



void cnnMainInit(VARNAME load_data_dir)
{
        #define _NCOL_ 4
        #define _NROW_ 4
        int minIF=0;
        int wsize=0;
        int minOF=0;
        int IG=0, OG=0;
VARNAME filename;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv0_param = spatconv_create();
 Conv0_param->pout =32;
 Conv0_param->pin = 1;

 Conv0_param->kern_s[0] = 32;
 Conv0_param->kern_s[1] = 1;
 Conv0_param->kern_s[2] = 5;
 Conv0_param->kern_s[3] = 5;

 Conv0_param->in_s[0] = 1;
 Conv0_param->in_s[1] = 32;
 Conv0_param->in_s[2] = 32;

 Conv0_param->out_s[0] = 32;
 Conv0_param->out_s[1] = 32;
 Conv0_param->out_s[2] = 32;

 Conv0_param->qf = 8;
 Conv0_param->activate = 0;
 Conv0_param->precision8 = 0;

Conv0_param->pad[0] = 2;
Conv0_param->pad[1] = 2;

Conv0_param->stride[0] = 1;
Conv0_param->stride[1] = 1;

Conv0_param->dil[0] = 1;
Conv0_param->dil[1] = 1;

 if (Conv0_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv0_param->pin/minIF;
 if (Conv0_param->pin%minIF)
     IG++;  

 OG= Conv0_param->pout/minOF;
 if (Conv0_param->pout%minOF)
     OG++;

 Conv0_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu0_param = relu_create();
Relu0_param->size[0] = 32;
Relu0_param->size[1] = 32;
Relu0_param->size[2] = 32;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool0_param = maxpool_create();
MaxPool0_param->in_s[0] = 32;
MaxPool0_param->in_s[1] = 32;
MaxPool0_param->in_s[2] = 32;
MaxPool0_param->out_s[0] = 32;
MaxPool0_param->out_s[1] = 16;
MaxPool0_param->out_s[2] = 16;
MaxPool0_param->kern_s[0] = 2;
MaxPool0_param->kern_s[1] = 2;
MaxPool0_param->stride[0] = 2;
MaxPool0_param->stride[1] = 2;
MaxPool0_param->pad[0] = 0;
MaxPool0_param->pad[1] = 0;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv1_param = spatconv_create();
 Conv1_param->pout =64;
 Conv1_param->pin = 32;

 Conv1_param->kern_s[0] = 64;
 Conv1_param->kern_s[1] = 32;
 Conv1_param->kern_s[2] = 5;
 Conv1_param->kern_s[3] = 5;

 Conv1_param->in_s[0] = 32;
 Conv1_param->in_s[1] = 16;
 Conv1_param->in_s[2] = 16;

 Conv1_param->out_s[0] = 64;
 Conv1_param->out_s[1] = 16;
 Conv1_param->out_s[2] = 16;

 Conv1_param->qf = 8;
 Conv1_param->activate = 0;
 Conv1_param->precision8 = 0;

Conv1_param->pad[0] = 2;
Conv1_param->pad[1] = 2;

Conv1_param->stride[0] = 1;
Conv1_param->stride[1] = 1;

Conv1_param->dil[0] = 1;
Conv1_param->dil[1] = 1;

 if (Conv1_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv1_param->pin/minIF;
 if (Conv1_param->pin%minIF)
     IG++;  

 OG= Conv1_param->pout/minOF;
 if (Conv1_param->pout%minOF)
     OG++;

 Conv1_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu1_param = relu_create();
Relu1_param->size[0] = 64;
Relu1_param->size[1] = 16;
Relu1_param->size[2] = 16;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool1_param = maxpool_create();
MaxPool1_param->in_s[0] = 64;
MaxPool1_param->in_s[1] = 16;
MaxPool1_param->in_s[2] = 16;
MaxPool1_param->out_s[0] = 64;
MaxPool1_param->out_s[1] = 8;
MaxPool1_param->out_s[2] = 8;
MaxPool1_param->kern_s[0] = 2;
MaxPool1_param->kern_s[1] = 2;
MaxPool1_param->stride[0] = 2;
MaxPool1_param->stride[1] = 2;
MaxPool1_param->pad[0] = 0;
MaxPool1_param->pad[1] = 0;


//######################################
//            CONVOLUTIONAL LAYER init  
// ######################################


 Conv2_param = spatconv_create();
 Conv2_param->pout =128;
 Conv2_param->pin = 64;

 Conv2_param->kern_s[0] = 128;
 Conv2_param->kern_s[1] = 64;
 Conv2_param->kern_s[2] = 5;
 Conv2_param->kern_s[3] = 5;

 Conv2_param->in_s[0] = 64;
 Conv2_param->in_s[1] = 8;
 Conv2_param->in_s[2] = 8;

 Conv2_param->out_s[0] = 128;
 Conv2_param->out_s[1] = 8;
 Conv2_param->out_s[2] = 8;

 Conv2_param->qf = 8;
 Conv2_param->activate = 0;
 Conv2_param->precision8 = 0;

Conv2_param->pad[0] = 2;
Conv2_param->pad[1] = 2;

Conv2_param->stride[0] = 1;
Conv2_param->stride[1] = 1;

Conv2_param->dil[0] = 1;
Conv2_param->dil[1] = 1;

 if (Conv2_param->kern_s[2] == 3 )
     minIF=_NCOL_*3;
 else
     minIF=_NCOL_;

 minOF=_NROW_;

 IG= Conv2_param->pin/minIF;
 if (Conv2_param->pin%minIF)
     IG++;  

 OG= Conv2_param->pout/minOF;
 if (Conv2_param->pout%minOF)
     OG++;

 Conv2_param->maxog= 4;




// ###############################################################
// #######             RELU LAYER init          #############
// #############################################################



 Relu2_param = relu_create();
Relu2_param->size[0] = 128;
Relu2_param->size[1] = 8;
Relu2_param->size[2] = 8;


// ###############################################################
// #######             MAXPOOL LAYER init          #############
// #############################################################

MaxPool2_param = maxpool_create();
MaxPool2_param->in_s[0] = 128;
MaxPool2_param->in_s[1] = 8;
MaxPool2_param->in_s[2] = 8;
MaxPool2_param->out_s[0] = 128;
MaxPool2_param->out_s[1] = 4;
MaxPool2_param->out_s[2] = 4;
MaxPool2_param->kern_s[0] = 2;
MaxPool2_param->kern_s[1] = 2;
MaxPool2_param->stride[0] = 2;
MaxPool2_param->stride[1] = 2;
MaxPool2_param->pad[0] = 0;
MaxPool2_param->pad[1] = 0;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm0_param = linear_create();
Gemm0_param->in_s = 2048;
Gemm0_param->out_s = 50;
Gemm0_param->in_s = 2048;
 Gemm0_param->qf = 8;


// #########################################
//        FULLY CONNECTED LAYER init *
//##########################################

Gemm1_param = linear_create();
Gemm1_param->out_s = 10;
Gemm1_param->in_s = 50;
 Gemm1_param->qf = 8;


// ##################################################################
// ######################## WEIGHTS LOADING #######################
// ################################################################

//	Only convolutional layers and FC or GEMM layers have weights and biases
//	Convolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the soc. 
//	The FC and GEMM weights and biases may be stored in any place 

sprintf(filename, "%s/Conv0_weights.bin", load_data_dir);
Conv0_param -> kernel = (DATA*)malloc(800 * sizeof(DATA));
load_fixed(filename, 800, Conv0_param -> kernel);
sprintf(filename, "%s/Conv0_biases.bin", load_data_dir);
Conv0_param -> bias = (DATA*)malloc(32 * sizeof(DATA));
load_fixed(filename, 32, Conv0_param -> bias);



sprintf(filename, "%s/Conv1_weights.bin", load_data_dir);
Conv1_param -> kernel = (DATA*)malloc(51200 * sizeof(DATA));
load_fixed(filename, 51200, Conv1_param -> kernel);
sprintf(filename, "%s/Conv1_biases.bin", load_data_dir);
Conv1_param -> bias = (DATA*)malloc(64 * sizeof(DATA));
load_fixed(filename, 64, Conv1_param -> bias);



sprintf(filename, "%s/Conv2_weights.bin", load_data_dir);
Conv2_param -> kernel = (DATA*)malloc(204800 * sizeof(DATA));
load_fixed(filename, 204800, Conv2_param -> kernel);
sprintf(filename, "%s/Conv2_biases.bin", load_data_dir);
Conv2_param -> bias = (DATA*)malloc(128 * sizeof(DATA));
load_fixed(filename, 128, Conv2_param -> bias);




Gemm0_param->weights = (DATA*)malloc(102400 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_weights.bin", load_data_dir);
load_fixed(filename, 102400, Gemm0_param->weights);
Gemm0_param->bias = (DATA*)malloc(50 * sizeof(DATA));
sprintf(filename, "%s/Gemm0_biases.bin", load_data_dir);
load_fixed(filename,50,Gemm0_param->bias);

Gemm1_param->weights = (DATA*)malloc(500 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_weights.bin", load_data_dir);
load_fixed(filename, 500, Gemm1_param->weights);
Gemm1_param->bias = (DATA*)malloc(10 * sizeof(DATA));
sprintf(filename, "%s/Gemm1_biases.bin", load_data_dir);
load_fixed(filename,10,Gemm1_param->bias);



// ########################################################
// ######################## MEMORY ALLOCATION #######################
// #######################################################

//	Each layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.
//	We assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.
//	By editing this you can customize the memory utilization

Conv0_param->input = (DATA*)malloc(1024*sizeof(DATA));
Conv0_param->output = (DATA*)malloc(32768*sizeof(DATA));
Relu0_param->output = (DATA*)malloc(32768*sizeof(DATA));
MaxPool0_param->output = (DATA*)malloc(8192*sizeof(DATA));
Conv1_param->output = (DATA*)malloc(16384*sizeof(DATA));
Relu1_param->output = (DATA*)malloc(16384*sizeof(DATA));
MaxPool1_param->output = (DATA*)malloc(4096*sizeof(DATA));
Conv2_param->output = (DATA*)malloc(8192*sizeof(DATA));
Relu2_param->output = (DATA*)malloc(8192*sizeof(DATA));
MaxPool2_param->output = (DATA*)malloc(2048*sizeof(DATA));
Gemm0_param->output = (DATA*)malloc(50*sizeof(DATA));
Gemm1_param->output = (DATA*)malloc(10*sizeof(DATA));



// ########################################################
// ######################## WIRING #######################
// #######################################################

//	Each layer has an input and an output pointer. The connection between two layers is defined here


Relu0_param->input = Conv0_param->output;
MaxPool0_param->input = Relu0_param->output;
Conv1_param->input = MaxPool0_param->output;
Relu1_param->input = Conv1_param->output;
MaxPool1_param->input = Relu1_param->output;
Conv2_param->input = MaxPool1_param->output;
Relu2_param->input = Conv2_param->output;
MaxPool2_param->input = Relu2_param->output;
DATA* Flatten0_dummy = MaxPool2_param->output;
Gemm0_param->input = Flatten0_dummy;
Gemm1_param->input = Gemm0_param->output;



}

//#########################################################################################################################################
//#########################################################################################################################################


void cnnMain(DATA* image, DATA* results)
{
SIZE input_size[] = {1,32,32}; 
SIZE batch_join_dim = 1 * 32 * 32;
for (int j=0;j<batch_join_dim;j++)
  Conv0_param->input[j]=image[j];



//################################################    Conv layer ###############################################
//kernel 5x5
int Conv0_job_id =spatconv_forward_wrap (Conv0_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu0_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool0_param);



//################################################    Conv layer ###############################################
//kernel 5x5
int Conv1_job_id =spatconv_forward_wrap (Conv1_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu1_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool1_param);



//################################################    Conv layer ###############################################
//kernel 5x5
int Conv2_job_id =spatconv_forward_wrap (Conv2_param);



//################################################# Relu layer ##########################################################
relu_forward_wrap (Relu2_param);



// ############################## MaxPool layer #############################################

maxpool_forward_wrap (MaxPool2_param);

//flatten 

//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm0_param);


//################################################# Linear layer ##########################################################
linear_forward_wrap(Gemm1_param);



for(unsigned int i = 0; i < 10; i++){
	results[i] = Gemm1_param->output[i];
	_dprintf_("[%d] %d \n", i, Gemm1_param->output[i]);
}
//exit(0); 
}

 

void getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){
// This function is executed for each file in the images folder.
// After you load the content of the file you can perform whatever preprocessing you want

  //source_forward(image_path, image_pixels, inSz, _QF_); // preprocesses JPG images 
  load_fixed(image_path,1024,image_pixels);// loads binary files

}


int resultsProcessing(DATA* results, int size){
//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)
//The simplest classifier is a maximum search for the results which returns the index value of the maximum

 char *labels[10]={"label_0", "label_1", "label_2", "label_3", "label_4", "label_5", "label_6", "label_7", "label_8", "label_9"};

  int top0=0;
  DATA topval=results[0];
  for (int i =1;i<size;i++){
    if (results[i]>topval){
      top0=i;
      topval=results[i];
    }  
  }
  printf("TOP 0: [%d] %s\n", top0, labels[top0]);
  return top0;
}
