// include the SoftwareSerial library so you can use its functions:
#include <SoftwareSerial.h>
#include <Servo.h>        // Include servo library

#define rxPin 10 
#define txPin 7
#define servoleft 13
#define servoright 12

void go(char *);
//char yes[] = "frfflfrrfrfflfs";
char yes[] = "ffrffflfffs";
char no[] = "ffrflfffs";
char down[] = "fflfrfffs";
char up[] = "fflfffrfffs";

//#define ledPin 13
//byte pinState = 0;

// set up a new serial port
SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);
char *words[]={"YES","NO","UP","DOWN"};

// Declare left and right servos
Servo servoLeft; 
Servo servoRight;

void setup()
{
  // define pin modes for tx, rx, led pins:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
//  pinMode(ledPin, OUTPUT);
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  mySerial.println("\r\nInput Serial Line ROBOTtino !!!");
  mySerial.print("Insert cmd (y, n, d, u, cr to finish):\t");

  Serial.begin(9600); 
  delay(5000);    // mininum delay to see to print on Serial (Serial.print)
  Serial.println("Serial Monitor ROBOTtinoooooo !!!");

  Serial.println("Servos setup !!!");
  servoLeft.attach(servoleft); // Attach left signal to P13
  servoRight.attach(servoright); // Attach right signal to P12
}

void loop() 
{
  String msg;
  msg.reserve(200);
  
  // listen for new serial coming in:
  if(mySerial.available() > 0){ // Wait for character
    msg = mySerial.readString();
    for(int i= 0; i < msg.length(); i++){
      Serial.print(msg[i], HEX);      
      Serial.print(" ");
    }
      Serial.println();
       
//    char myChar = mySerial.read();
    char myChar = msg[0];
    mySerial.println(msg);  // echo on mySerial
    if(myChar != 0x0D){
      
      Serial.println("\t\t\tSTART DELAY");
      delay(5000);
      
      Serial.print("Received on mySerial: ");
      Serial.println(myChar); 
//      mySerial.write(myChar);   // echo
      switch(myChar) // Switch to code 
      {
        case 'y':
          Serial.print("Recognized word: ");
          Serial.print(words[0]);
          Serial.print(" sz(");
          Serial.print(sizeof(yes)-2);
          Serial.println(")");
          go(yes);
          break;
        case 'n':
          Serial.print("Recognized word: ");
          Serial.print(words[1]);
          Serial.print(" sz(");
          Serial.print(sizeof(no)-2);
          Serial.println(")");
          go(no);
          break;        
        case 'u':
          Serial.print("Recognized word: ");
          Serial.print(words[2]);
          Serial.print(" sz(");
          Serial.print(sizeof(up)-2);
          Serial.println(")");
          go(up);
          break;
        case 'd':
          Serial.print("Recognized word: ");
          Serial.print(words[3]);
          Serial.print(" sz(");
          Serial.print(sizeof(down)-2);
          Serial.println(")");
          go(down);
          break;        
        default:
          Serial.println("Word not recognized !!!");
      }
    } 
    mySerial.print("Insert cmd (y, n, d, u, cr to finish):\t");
  }   
  delay(1000);    
}

void go(char *cmd_list) // go function
{
  Serial.println("Start GO !!!");
  Serial.print("\t");
  Serial.println(cmd_list);
  Serial.print("\t");
  int index = 0;
  char c = cmd_list[index++];
  do
  {
    switch(c) // Switch to based on c
    {
      case 'f': // c contains 'f'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1700); // Full speed forward
        servoRight.writeMicroseconds(1300);
        delay(1000); // Execute for 1 s (about 15,5 cm)
        break;
      case 'b': // c contains 'b'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1300); // Full speed backward
        servoRight.writeMicroseconds(1700);
        delay(1000); // Execute for 1 s
        break;
      case 'l': // c contains 'l'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1300); // Rotate left in place
        servoRight.writeMicroseconds(1300);
        delay(560); // Execute about 90 degrees angle
        break;
      case 'r': // c contains 'r'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1700); // Rotate right in place
        servoRight.writeMicroseconds(1700);
        delay(560); // Execute about 90 degrees angle
        break;
      case 'L': // c contains 'L'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1500); // Pivot left in place
        servoRight.writeMicroseconds(1300);
        delay(1070);       // 1070=90°
        break;
      case 'R': // c contains 'R'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1700); // Pivot right in place
        servoRight.writeMicroseconds(1500);
        delay(1060);       // 1060=90°
        break;
/*        
      case 's': // c contains 's'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1500); // Stop
        servoRight.writeMicroseconds(1500);
        delay(1000); // Execute for 1 s
        break;
*/
      case 'h': // c contains 'h'
        Serial.print(c);
        Serial.print("\t");
        servoLeft.writeMicroseconds(1550); // Half speed forward
        servoRight.writeMicroseconds(1450);
        break;
    }
  } while((c = cmd_list[index++]) != 's');
  servoLeft.writeMicroseconds(1500); // Stop
  servoRight.writeMicroseconds(1500);
  delay(1000); // Execute for 1 s
  Serial.println("\nEnd GO !!!");
}
