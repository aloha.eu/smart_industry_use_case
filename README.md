# Smart Industry Use Case

## Scripts
The `scripts` folder contains the scripts that can be used to:

 * create a dataset file starting from the complete Google Speech Dataset
 * obtain an offline augmentation of the dataset
 * split in training and validation sets a dataset file obtained with one of the previous scripts
 
 The split put recordings from the same speaker in the same set.
