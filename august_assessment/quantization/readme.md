# Quantization

Move to refinement_for_parsimonius_inference

Checkout branch reply

Move to refinement_for_parsimonius_inference/app/rpi_engine

Checkout branch reply

Clone onnxparser in rpi_engine and then checkout branch reply

Add plugins and onnparser to python path

`export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/shared_data"`

`export PYTHONPATH="${PYTHONPATH}:/home/lrinelli/20200219_ALOHA/refinement_for_parsimonious_inference/app/rpi_engine/onnxparser"`

dataset.json is the json from the entry in the dataset collection in the mongo db from which all the fields except slice_split have been removed, ./audio_to_mel_3216.json contains the document from the db for the pipeline

`python3 rpi_engine.py --onnx_path 7b.onnx --regime regime.json --log_dir . --export --dataset test_reply_projectID-5f2c1397394894ac14b96a94.hdf5 --split_json dataset.json --path_preprocessing_pipeline_instance ./audio_to_mel_3216.json --input_shape "[1,32,16]"`