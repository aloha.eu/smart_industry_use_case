import hashlib
import argparse

MAX_NUM_WAVS_PER_CLASS = 2**27-1

def split_kws(filename, output_file_train, output_file_valid, split, ignore_target, seed):
    with open(filename) as f:
        content = f.readlines()

    training_dataset = []
    validation_dataset = []
    line_processed = 0

    for line in content:
        [path, target] = line.rstrip().split(' ')

        if target not in ignore_target:
            speaker_id = path.split('/')[-1].split('_')[0]
            hash_name_hashed = hashlib.sha1((seed+speaker_id).encode('utf-8')).hexdigest()
        else:
            hash_name_hashed = hashlib.sha1((seed+path.split('/')[-1]).encode('utf-8')).hexdigest()

        percentage_hash = ((int(hash_name_hashed, 16) %
                            (MAX_NUM_WAVS_PER_CLASS + 1)) *
                        (1 / MAX_NUM_WAVS_PER_CLASS))

        if percentage_hash < 0.7:
            training_dataset = training_dataset + [{'path': path, 'target': target}]
        else:
            validation_dataset = validation_dataset + [{'path': path, 'target': target}]

        line_processed = line_processed + 1

    print("complete dataset size is", line_processed)
    print("training dataset length is", len(training_dataset), "instead of", int(split*line_processed))
    print("validation dataset length is", len(validation_dataset), "instead of", int((1-split)*line_processed))

    f = open(output_file_train, 'w')
    for line in training_dataset:
        f.write(line['path']+" "+line['target']+"\n")
    f.close()

    f = open(output_file_valid, 'w')
    for line in validation_dataset:
        f.write(line['path']+" "+line['target']+"\n")
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Split dataset file in two files, one for training and another for validation. The program puts all recordings from the same speaker in the same set.')
    parser.add_argument('input_file', type=str, help='Path to file to be splitted.')
    parser.add_argument('output_training_file', type=str, help='Output file containing training split.')
    parser.add_argument('output_validation_file', type=str, help='Output file containing validation split.')
    parser.add_argument('split', type=float, help='Training set portion, from 0 to 1')
    parser.add_argument('-i', '--ignore_targets', dest='ignore_targets', type=int, help='List of targets with no speaker id', nargs='+')
    parser.add_argument('-s', '--seed', dest='random_seed', type=str, help='String random seed.', default=None, required=False)

    args = parser.parse_args()
    split_kws(args.input_file, args.output_training_file, args.output_validation_file, args.split, args.ignore_targets, args.random_seed)