import argparse
from bson import json_util, ObjectId
import json
from pymongo import MongoClient
from datetime import datetime, tzinfo, timezone
#import pprint

def query_database():
    client = MongoClient('mongodb://aloha.cloud.reply.eu:27018')
    result = client['aloha2']['project'].aggregate([
        {
            '$match': {
                'creation_date': {
                    '$gte': datetime(2020, 9, 20, 14, 56, 43, tzinfo=timezone.utc), 
                    '$lte': datetime(2020, 9, 29, 11, 11, 47, tzinfo=timezone.utc)
                }
            }
        }, {
            '$addFields': {
                'id': {
                    '$toString': '$_id'
                }
            }
        }, {
            '$lookup': {
                'from': 'configuration_file', 
                'localField': 'id', 
                'foreignField': 'project', 
                'as': 'configuration_file'
            }
        }, {
            '$lookup': {
                'from': 'algorithm_configuration', 
                'localField': 'id', 
                'foreignField': 'project', 
                'as': 'algorithm_configuration'
            }
        }, {
            '$lookup': {
                'from': 'pipeline', 
                'localField': 'configuration_file.pipeline', 
                'foreignField': '_id', 
                'as': 'pipeline'
            }
        }
    ])

    #pprint.pprint(list(result))
    print("Dumping json")
    with open('experiments_data.json', 'w') as outfile:
    	sanitized = json.loads(json_util.dumps(list(result)))
    	json.dump(sanitized, outfile)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract results')
    #parser.add_argument('dataset_folder', type=str, help='Folder containing the folders containing the dataset samples.')
    #parser.add_argument('output_file', type=str, help='Output dataset file.')
    #parser.add_argument('aloha_folder', type=str, help='Dataset folder path inside ALOHA toolflow.')
    #parser.add_argument('number_of_samples', type=int, help='Number of samples to add per class.')
    #parser.add_argument('keywords_json', type=str, help='Path to json file containing selected keywords')
    #parser.add_argument('-s', '--seed', dest='random_seed', type=int, help='Integer random seed.', default=None, required=False)

    #args = parser.parse_args()
    query_database()
    
